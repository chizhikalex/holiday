package zsoft.net.holiday;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.ORM.HelperFactory;
import zsoft.net.holiday.categories.Category;
import zsoft.net.holiday.goods.Good;
import zsoft.net.holiday.news.News;
import zsoft.net.holiday.present.Present;

/**
 * Created by Dimon on 02.07.2015.
 */
public final class App extends Application {

        private List<News> news;
        private List<Present> presents;
        private List<Category> categories;
        private Present firstPresents;
        private List<Good> goods;
        private List<Category>  tempCategories = new ArrayList<Category>();
        VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
                @Override
                public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
                        if (newToken == null) {
// VKAccessToken is invalid
                        }
                }
        };
        @Override
        public void onCreate() {
                super.onCreate();
                vkAccessTokenTracker.startTracking();
                VKSdk.initialize(this);

        }

        @Override
        public void onTerminate() {
                HelperFactory.releaseHelper();
                super.onTerminate();
        }


        public void setNews(List<News> news) {
                this.news = news;
        }

        public List<News> getNews() {
                return news;
        }

        public List<Present> getPresents() {
                return presents;
        }

        public void setPresents(List<Present> presents) {
                this.presents = presents;
        }

        public List<Category> getCategories() {
                return categories;
        }

        public void setCategories(List<Category> categories) {
                this.tempCategories.clear();
                this.tempCategories.addAll(categories);
                this.categories = categories;
        }


        public void setCustomActionBar(final AppCompatActivity activity) {
                if(android.os.Build.VERSION.SDK_INT>=21) {
                        colorizeInfoBar(activity);
                }


                Display display = activity.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int behindWidth = (size.x / 2) + ((size.x / 2) / 2);



               // activity.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
                ActionBar mActionBar = activity.getSupportActionBar();
                mActionBar.setDisplayShowHomeEnabled(false);
                mActionBar.setDisplayShowTitleEnabled(false);

                LayoutInflater mInflater = LayoutInflater.from(this);

                View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
                mCustomView.findViewById(R.id.slideMenuBtn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                if (MainActivity.menu.isMenuShowing()) {
//                                        MainActivity.menu.toggle(true);
//                                } else {
//                                        MainActivity.menu.showMenu(true);
//                                }

                                activity.getFragmentManager().popBackStack();

                        }
                });
                TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
                mTitleTextView.setText("Holyday");

                ImageButton imageButton = (ImageButton) mCustomView.findViewById(R.id.imageButton);
                imageButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                                Toast.makeText(getApplicationContext(), "Star Clicked!", Toast.LENGTH_LONG).show();
                        }
                });

                mActionBar.setCustomView(mCustomView);
                mActionBar.setDisplayShowCustomEnabled(true);
                Toolbar parent =(Toolbar) mCustomView.getParent();
                parent.setContentInsetsAbsolute(0, 0);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        public void colorizeInfoBar(Activity activity) {
                Window window = activity.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(Color.parseColor("#051139"));
        }

        public void setFirstPresents(Present firstPresents) {

                this.firstPresents = firstPresents;
        }

        public Present getFirstPresent() {
                return this.firstPresents;
        }

        public void setGoods(List<Good> goods) {
                this.goods = goods;
        }

        public void sortBarCategories(boolean isBar){
                this.categories.clear();
               for(Category category : tempCategories){
                       if(category.isbar() == isBar){
                               categories.add(category);
                       }
               }
        }


        public List<Good> getGoods(int id) {
                ArrayList arrayList = new ArrayList();
                for (Good good : goods) {
                        if (good.getCategoriesid() == id) {
                                arrayList.add(good);
                        }
                }
                return arrayList;
        }
}