package zsoft.net.holiday.Others;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.Fragments.MapsFragment;
import zsoft.net.holiday.R;

/**
 * Created by Sergey on 9/9/15.
 */
public class OtherGridFragment extends Fragment {
    private View view;
    private OtherGridAdapter adapter;
    private List<Other> others;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_oters, null);

        GridView gvOthers = (GridView) view.findViewById(R.id.gvOthers);
        generatData();


        adapter = new OtherGridAdapter(getActivity(), others);
        gvOthers.setAdapter(adapter);

        gvOthers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame,new NewsDescriptionFragment().setNews(adapter.getItem(position))).addToBackStack(null).commit();
                String title = adapter.getItem(position).getTitle();
                if (title.equals("Мы на карте")) {

                    getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new MapsFragment()).addToBackStack(null).commit();
                    //((MainActivity) getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new MapsFragment()).addToBackStack(null).commit();

                } else {
                    if (title.equals("Позвонить нам")) {
                        View dialog = getActivity().getLayoutInflater().inflate(R.layout.call_dialog, null);

                        final Dialog callDialog = new Dialog(getActivity());
                        callDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        callDialog.setContentView(dialog);
                        callDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        callDialog.show();

                        dialog.findViewById(R.id.btn099).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String uri = "tel:+380991777244";
                                Intent intent = new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse(uri));
                                startActivity(intent);
                            }
                        });

                        dialog.findViewById(R.id.btn093).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String uri = "tel:+380930441674";
                                Intent intent = new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse(uri));
                                startActivity(intent);
                            }
                        });

                        dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callDialog.cancel();
                            }
                        });

                    } else {
                        if (title.equals("Оставь свой отзыв, мгазь")) {

                        } else {
                           Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.night-museum.com.ua/#!/page_home"));
                            startActivity(intent);

                        }


                    }

                }


            }
        });


        return view;
    }

    private void generatData() {
        others=new ArrayList<Other>();
        others.add(new Other().setTitle("Мы на карте").setImagepath(R.drawable.we_on_map));
        others.add(new Other().setTitle("Позвонить нам").setImagepath(R.drawable.call_us));
        others.add(new Other().setTitle("Оставь свой отзыв, мгазь").setImagepath(R.drawable.feedback));
        others.add(new Other().setTitle("Зырь какой моднявый сайт").setImagepath(R.drawable.internet));
    }


}
