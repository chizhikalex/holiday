package zsoft.net.holiday.Others;

/**
 * Created by Sergey on 9/9/15.
 */
public class Other {
    public String getTitle() {
        return title;
    }

    public Other setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getImagepath() {
        return imagepath;
    }

    public Other setImagepath(int imagepath) {
        this.imagepath = imagepath;
        return this;
    }

    private String title;
    private int imagepath;
}
