package zsoft.net.holiday.Others;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.util.List;

import zsoft.net.holiday.GrandAdapter;
import zsoft.net.holiday.R;

/**
 * Created by Sergey on 9/9/15.
 */
public class OtherGridAdapter extends GrandAdapter<Other> {
    private final DisplayImageOptions options;
    private ImageLoader imageLoader;
    public OtherGridAdapter(Context mainActivity, List<Other> list) {
        super(mainActivity, list, R.layout.others_grid_item);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mainActivity)
                .memoryCacheExtraOptions(320, 640) // default = device screen dimensions
                .diskCacheExtraOptions(320, 640, null)
                .threadPoolSize(4) // default
                .build();
        ImageLoader.getInstance();//.init(config);

        options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(false)  // default
                .delayBeforeLoading(0)
                .cacheInMemory(true) // default
                .cacheOnDisk(true) // default
                .build();

        imageLoader = ImageLoader.getInstance();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder = new ViewHolder();

        if (view == null || convertView.getTag() == null) {
            view = getLayout();
            holder.ivImage = (ImageView) view.findViewById(R.id.ivImage);
            holder.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            view.setTag(holder);
        }

        if (getItem(position).getTitle() == null){
            return view;
        }

        holder = (ViewHolder) view.getTag();

        holder.tvTitle.setText(getItem(position).getTitle());
        //imageLoader.displayImage(getItem(position).getImagepath(), holder.ivImage, options);
        //imageLoader.displayImage("drawable://" + getItem(position).getImagepath(), holder.ivImage);
        Picasso.with(getContext()).load(getItem(position).getImagepath()).into(holder.ivImage);
        return view;
    }

    static class ViewHolder {
        TextView tvTitle;
        ImageView ivImage;
    }
}
