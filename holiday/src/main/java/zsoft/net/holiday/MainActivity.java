package zsoft.net.holiday;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.vk.sdk.util.VKUtil;

import java.lang.reflect.Field;
import java.util.Arrays;

import zsoft.net.holiday.Fragments.LoadingFragment;
import zsoft.net.holiday.Fragments.MainFragment;
import zsoft.net.holiday.ORM.HelperFactory;

public final class MainActivity extends AppCompatActivity {

    public App application;
    public DrawerLayout mDrawerLayout;
    public static MainActivity activity;
    public static boolean isDescriptionScreen;
    @Override
    public void onBackPressed() {
        Log.d("tyuiiii","");

        if (getFragmentManager().getBackStackEntryCount() == 0) {
            showExitLogin();
            //super.onBackPressed();
        } else {
            mDrawerLayout.closeDrawer(GravityCompat.START);

            getFragmentManager().popBackStack();
//            if(menu.isMenuShowing()) {
//                menu.toggle(true); or hide this shit?
//            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=this;
        getSupportActionBar().hide();
        HelperFactory.setHelper(getApplicationContext());
        setContentView(R.layout.activity_main_with_fragment);
        FacebookSdk.sdkInitialize(getApplicationContext());
        Configuration configuration = getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp;
        Log.d("1", Arrays.deepToString(VKUtil.getCertificateFingerprint(this, this.getPackageName()))+screenWidthDp);

//		LayoutInflater li=(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View yourview=li.inflate(R.layout.custom_actionbar, null);
//		((LinearLayout)findViewById(R.id.actionBarLayout)).addView(yourview);
        application = ((App) getApplication());
        application.setCustomActionBar(this);



        // configure the SlidingMenu

//        menu = new SlidingMenu(this);
//        menu.setMode(SlidingMenu.LEFT);
//        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//        menu.setShadowDrawable(R.drawable.shadow);
//        menu.setShadowWidthRes(R.dimen.shadow_width);
//        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
//        menu.setFadeDegree(0.35f);
//        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT, false);
//        menu.setMenu(R.layout.menu);
//
//        FragmentTransaction t = getFragmentManager().beginTransaction();
//        t.replace(R.id.main, new MenuListFragment()).commit();
        generateDrawer();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new LoadingFragment()).commit();
        //getFragmentManager().beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();

        int i = 974 * 123123;

        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        Log.d("FINGERPRINT", Arrays.toString(fingerprints));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoginManager.getInstance().logOut();
        HelperFactory.releaseHelper();

    }

    private void showExitLogin() {
        final Dialog loginDialog = new Dialog(this);
        View.OnClickListener onClickLoginDialog = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.btnAccept) {
                    System.exit(1);
                } else
                    loginDialog.cancel();
            }
        };

        loginDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loginDialog.setContentView(R.layout.exit_dialog);
        loginDialog.findViewById(R.id.btnAccept).setOnClickListener(onClickLoginDialog);
        loginDialog.findViewById(R.id.btnCancel).setOnClickListener(onClickLoginDialog);
        loginDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loginDialog.show();
    }


    private void generateDrawer(){



        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        FrameLayout mDrawerList = (FrameLayout) findViewById(R.id.left_drawer);
        FrameLayout rawerList = (FrameLayout) findViewById(R.id.right_drawer);

//        //int width = getResources().getDisplayMetrics().widthPixels/2;
//        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerList.getLayoutParams();
//        DrawerLayout.LayoutParams arams = (android.support.v4.widget.DrawerLayout.LayoutParams) rawerList.getLayoutParams();
//        params.width=  (int)dipToPixels(this,getResources().getDimension(R.dimen.slidingmenu_offset));
//        arams.width=  (int)dipToPixels(this,getResources().getDimension(R.dimen.slidingmenu_offset));
//        //params.width = width;
//        mDrawerList.setLayoutParams(params);
//        rawerList.setLayoutParams(arams);


        Field mLeftDragger = null;
        try {
            mLeftDragger = mDrawerLayout.getClass().getDeclaredField(
                    "mLeftDragger");//mRightDragger for right obviously
            mLeftDragger.setAccessible(true);
            ViewDragHelper draggerObj = (ViewDragHelper) mLeftDragger
                    .get(mDrawerLayout);

            Field mEdgeSize = draggerObj.getClass().getDeclaredField(
                    "mEdgeSize");
            mEdgeSize.setAccessible(true);
            int edge = mEdgeSize.getInt(draggerObj);

            mEdgeSize.setInt(draggerObj, edge * 5 );
        } catch (Exception e) {
            e.printStackTrace();
        }


        Field mRightDragger = null;
        try {
            mRightDragger = mDrawerLayout.getClass().getDeclaredField(
                    "mRightDragger");//mRightDragger for right obviously
            mRightDragger.setAccessible(true);
            ViewDragHelper draggerObj = (ViewDragHelper) mRightDragger
                    .get(mDrawerLayout);

            Field mEdgeSize = draggerObj.getClass().getDeclaredField(
                    "mEdgeSize");
            mEdgeSize.setAccessible(true);
            int edge = mEdgeSize.getInt(draggerObj);

            mEdgeSize.setInt(draggerObj, edge * 5);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                if(drawerView.equals(findViewById(R.id.left_drawer))) {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
                }else {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,GravityCompat.START );
                }
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                if(drawerView.equals(findViewById(R.id.right_drawer))) {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.START);
                }else {
                    if(isDescriptionScreen)
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED,Gravity.RIGHT );
                }
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

    }

    public void afterLoading(){

        FragmentManager fragmentManager = getFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.content_frame,new MainFragment()).commitAllowingStateLoss() ;
        fragmentManager.beginTransaction().replace(R.id.left_drawer,new MenuListFragment()).commitAllowingStateLoss();
        fragmentManager.beginTransaction().replace(R.id.right_drawer,new InfoFragment()).commitAllowingStateLoss();
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
    }

    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

}
