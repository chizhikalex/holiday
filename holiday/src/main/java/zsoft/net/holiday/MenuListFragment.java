package zsoft.net.holiday;

import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import zsoft.net.holiday.Fragments.AboutUsFragment;
import zsoft.net.holiday.Fragments.MainFragment;
import zsoft.net.holiday.Fragments.ProfileFragment;

public final class MenuListFragment extends ListFragment {
    public static final String MENU_HOME = "Главная";
    public static final String MENU_ABOUT_US = "О нас";
    public static final String MENU_SHARE = "Поделитса с другом";
    public static final String MENU_SUPPORT = "Техническая поддержка";
    public static final String MENU_SETTINGS = "Настройки";
    public static final String MENU_LOGOUT = "Сменить аккаунт";

    public static void setLastMenuItem(String lastMenuItem) {
        MenuListFragment.lastMenuItem = lastMenuItem;
    }

    private static String lastMenuItem = MENU_HOME;
    public static View view ;

    private MenuAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_slide_menu, null);
        if(SharedPreferencesAPI.getId()==-1){
            return view;
        }
        ImageLoader.getInstance().displayImage(SharedPreferencesAPI.getImage(), (ImageView)view.findViewById(R.id.ivLogo));
        ((TextView) MenuListFragment.view.findViewById(R.id.textView)).setText(SharedPreferencesAPI.getName());

        return view;
    } // onCreateView()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new MenuAdapter(getActivity());

        adapter.add(new MenuItem(MENU_HOME, R.drawable.home1));
        adapter.add(new MenuItem(MENU_ABOUT_US, R.drawable.aboutus1));
       // adapter.add(new MenuItem(MENU_SHARE, R.drawable.share1));
        adapter.add(new MenuItem(MENU_SUPPORT, R.drawable.icon_support1));
        adapter.add(new MenuItem(MENU_SETTINGS, R.drawable.redactirovanie));
        adapter.add(new MenuItem(MENU_LOGOUT,R.drawable.logout));
        setListAdapter(adapter);
    } // onActivityCreated()

    private class MenuItem {
        public String tag;
        public int iconRes;

        public MenuItem(String tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }
    } // Class MenuItem

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (adapter.getItem(position).tag.equals(MENU_HOME)) {
            ((MainActivity) getActivity()).mDrawerLayout.closeDrawer(Gravity.LEFT);
            if(!lastMenuItem.equals(MENU_HOME)) {
                clearBackStack();
                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
                //getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                lastMenuItem = MENU_HOME;
            }
            return;
        }

        if (adapter.getItem(position).tag.equals(MENU_ABOUT_US)) {
//			Intent intent = new Intent(getActivity(), AboutUsActivity.class);
//			startActivity(intent);
            //MainActivity.menu.toggle(true);
            //getActivity().getFragmentManager().getFragment(getActivity(),null)
            ((MainActivity) getActivity()).mDrawerLayout.closeDrawer(Gravity.LEFT);
            //clearBackStack();
            if(!lastMenuItem.equals(MENU_ABOUT_US)) {
                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new AboutUsFragment()).addToBackStack(null).commit();
                lastMenuItem = MENU_ABOUT_US;
                return;
            }
            //getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if (adapter.getItem(position).tag.equals(MENU_SETTINGS)) {
            //MainActivity.menu.toggle(true);
            //getActivity().getFragmentManager().getFragment(getActivity(),null)
            ((MainActivity) getActivity()).mDrawerLayout.closeDrawer(Gravity.LEFT);
            //clearBackStack();
            if(!lastMenuItem.equals(MENU_SETTINGS)) {
                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new ProfileFragment()).addToBackStack(null).commit();
                lastMenuItem = MENU_SETTINGS;
                ////getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingsFragment()).addToBackStack(null).commit();
            }
            return;
            //getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if (adapter.getItem(position).tag.equals(MENU_LOGOUT)) {
//			((MainActivity) getActivity()).mDrawerLayout.closeDrawer(Gravity.LEFT);


            ((MainActivity) getActivity()).mDrawerLayout.closeDrawer(Gravity.LEFT);
            if(!lastMenuItem.equals(MENU_LOGOUT)) {
                SharedPreferencesAPI.saveAuth(-1, "", "", "", "");
                clearBackStack();
                // MainFragment.
                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
                lastMenuItem = MENU_LOGOUT;
            }
            //getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            return;
        }

    } // onListItemClick()

    public class MenuAdapter extends ArrayAdapter<MenuItem> {

        public MenuAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_slide_menu, null);
            }
            ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
            icon.setImageResource(getItem(position).iconRes);
            TextView title = (TextView) convertView.findViewById(R.id.row_title);
            title.setText(getItem(position).tag);
            return convertView;
        } // getView()

    } // Class MenuAdapter

    private  void clearBackStack() {
        FragmentManager fragmentManager =  getActivity().getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

}