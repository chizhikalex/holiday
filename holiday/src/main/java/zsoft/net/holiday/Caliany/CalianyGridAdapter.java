package zsoft.net.holiday.Caliany;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import zsoft.net.holiday.GrandAdapter;
import zsoft.net.holiday.R;
import zsoft.net.holiday.Server;

/**
 * Created by Sergey on 9/18/15.
 */
public class CalianyGridAdapter extends GrandAdapter<Calian> {

    public CalianyGridAdapter(Context mainActivity, List<Calian> list) {
        super(mainActivity, list, R.layout.item_good_grid);


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder holder = new ViewHolder();

        if (view == null || convertView.getTag() == null) {
            view = getLayout();
            holder.ivImage = (ImageView) view.findViewById(R.id.ivImage);
            holder.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            holder.tvPay = (TextView) view.findViewById(R.id.tvPay);
            view.setTag(holder);
        }

        holder = (ViewHolder) view.getTag();
        holder.tvTitle.setText(getItem(position).getTitle());
        holder.tvPay.setText(getItem(position).getPrice() + "");
        Log.d("456", getItem(position).getImagepath());
        ContextWrapper cw = new ContextWrapper(getContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir(Server.CALIANY, Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,getItem(position).getImagepath());
        Picasso.with(getContext()).load(mypath).into(holder.ivImage);

        //imageLoader.displayImage("file://"+mypath, holder.ivImage, options);
        return view;
    }

    static class ViewHolder {
        TextView tvTitle;
        TextView tvPay;
        ImageView ivImage;
    }
}