package zsoft.net.holiday.Caliany;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Sergey on 9/18/15.
 */
@DatabaseTable(tableName = "caliany")
public class Calian implements Serializable {

    @DatabaseField(id = true,dataType = DataType.INTEGER)
    private int id;
    @DatabaseField( dataType = DataType.STRING)
    private String title;
    @DatabaseField( dataType = DataType.STRING)
    private String description;
    @DatabaseField( dataType = DataType.STRING)
    private String moredescription;
    @DatabaseField( dataType = DataType.STRING)
    private String imagepath;
    @DatabaseField( dataType = DataType.INTEGER)
    private int price;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMoredescription() {
        return moredescription;
    }

    public void setMoredescription(String moredescription) {
        this.moredescription = moredescription;
    }


    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
        //this.imagepath = "http://holiday.pe.hu/img/" + imagepath;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }
}
