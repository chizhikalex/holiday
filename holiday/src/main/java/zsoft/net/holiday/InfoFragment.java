package zsoft.net.holiday;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Dimon on 02.09.2015.
 */
public class InfoFragment extends Fragment {

    public String getTitle() {
        return title;
    }

    public InfoFragment setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;

    }

    public InfoFragment setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getMoredescription() {
        return moredescription;
    }

    public InfoFragment setMoredescription(String moredescription) {
        this.moredescription = moredescription;
        return this;
    }

    private String title;
    private String description;
    private String moredescription;
    public static View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_info, null);
        ((TextView)view.findViewById(R.id.textView25)).setText(description);
        if(moredescription==null||moredescription.length()==0){
            view.findViewById(R.id.textView26).setVisibility(View.GONE);
            view.findViewById(R.id.textView27).setVisibility(View.GONE);
        }else{
            ((TextView)  view.findViewById(R.id.textView27)).setText(moredescription);
        }

        return view ;
    }
}
