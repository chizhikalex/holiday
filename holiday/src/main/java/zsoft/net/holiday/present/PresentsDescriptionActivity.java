package zsoft.net.holiday.present;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import zsoft.net.holiday.App;
import zsoft.net.holiday.R;

/**
 * Created by Dimon on 02.07.2015.
 */
public final class PresentsDescriptionActivity extends AppCompatActivity {

    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presents_description);

        application = ((App) getApplication());
        //application.setCustomActionBar(this);

        Present present = (Present) getIntent().getSerializableExtra("present");

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(320, 640) // default = device screen dimensions
                .diskCacheExtraOptions(320, 640, null)
                .threadPoolSize(4) // default
                .build();
        ImageLoader.getInstance();//.init(config);
        ImageLoader imageLoader = ImageLoader.getInstance();


        ImageView ivImage = (ImageView) findViewById(R.id.ivImage);
        TextView tvPrice = (TextView) findViewById(R.id.tvPrice);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) findViewById(R.id.tvDescription);

        imageLoader.displayImage(present.getImagepath(), ivImage);
        tvPrice.setText(present.getPrice() + "");
        tvTitle.setText(present.getTitle());
        tvDescription.setText(present.getDescription());




    }
}