package zsoft.net.holiday.present;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import zsoft.net.holiday.App;
import zsoft.net.holiday.R;

public final class PresentsGridActivity extends AppCompatActivity {

    private PresentsGridAdapter adapter;
    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presents);

        application = ((App) getApplication());
        //application.setCustomActionBar(this);

        GridView gvPresents = (GridView) findViewById(R.id.gvPresents);

        adapter = new PresentsGridAdapter(this, ((App)getApplication()).getPresents());

        gvPresents.setAdapter(adapter);

        gvPresents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), PresentsDescriptionActivity.class);
                intent.putExtra("present", adapter.getItem(position));
                startActivity(intent);
            }
        });

    }
}
