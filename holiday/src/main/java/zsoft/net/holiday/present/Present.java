package zsoft.net.holiday.present;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
@DatabaseTable(tableName = "presents")
public final class Present implements Serializable{
	@DatabaseField(id = true,dataType = DataType.INTEGER)
	private int id;
	@DatabaseField( dataType = DataType.STRING)
	private String title;
	@DatabaseField( dataType = DataType.STRING)
	private String description;
	@DatabaseField( dataType = DataType.STRING)
	private String imagepath;
	@DatabaseField( dataType = DataType.INTEGER)
	private int price;

	public Present() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath =  imagepath;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "Present{" +
				"id=" + id +
				", title='" + title + '\'' +
				", description='" + description + '\'' +
				", imagepath='" + imagepath + '\'' +
				", price=" + price +
				'}';
	}
}
