package zsoft.net.holiday.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import zsoft.net.holiday.App;
import zsoft.net.holiday.R;

public final class NewsGridActivity extends AppCompatActivity {

    private NewsGridAdapter adapter;
    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        application = ((App) getApplication());
        //application.setCustomActionBar(this);

        GridView gvNews = (GridView) findViewById(R.id.gvNews);
        adapter = new NewsGridAdapter(this, ((App)getApplication()).getNews());
        gvNews.setAdapter(adapter);

        gvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(NewsGridActivity.this, NewsDescriptionActivity.class);
                intent.putExtra("neww", adapter.getItem(position));
                startActivity(intent);
            }
        });

    }
}
