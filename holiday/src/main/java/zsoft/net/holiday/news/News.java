package zsoft.net.holiday.news;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
@DatabaseTable(tableName = "news")
public final class News implements Serializable {

	@DatabaseField(id = true,dataType = DataType.INTEGER)
	private int id;
	@DatabaseField( dataType = DataType.STRING)
	private String title;
	@DatabaseField( dataType = DataType.STRING)
	private String description;
	@DatabaseField( dataType = DataType.STRING)
	private String imagepath;
	@DatabaseField( dataType = DataType.STRING)
	private String fromdate;
	@DatabaseField( dataType = DataType.STRING)
	private String todate;
	@DatabaseField( dataType = DataType.STRING)
	private String when;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public void setWhen(String when) {
		this.when = when;
	}

	public String getWhen() {
		return when;
	}
}
