package zsoft.net.holiday.news;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import zsoft.net.holiday.App;
import zsoft.net.holiday.R;

public final class NewsDescriptionActivity extends AppCompatActivity {

    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_description);

        application = ((App) getApplication());
        //application.setCustomActionBar(this);

       News neww = (News) getIntent().getSerializableExtra("neww");

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(320, 640) // default = device screen dimensions
                .diskCacheExtraOptions(320, 640, null)
                .threadPoolSize(4) // default
                .build();
        ImageLoader.getInstance();//.init(config);
        ImageLoader imageLoader = ImageLoader.getInstance();


        ImageView ivImage = (ImageView) findViewById(R.id.ivImage);
        TextView tvWhen = (TextView) findViewById(R.id.tvWhen);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) findViewById(R.id.tvDescription);

        imageLoader.displayImage(neww.getImagepath(), ivImage);
        tvWhen.setText(neww.getWhen() );
        tvTitle.setText(neww.getTitle());
        tvDescription.setText(neww.getDescription());




    }
}
