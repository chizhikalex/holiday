package zsoft.net.holiday;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import zsoft.net.holiday.present.Present;

/**
 * Created by sergey on 8/17/15.
 */
public class SharedPreferencesAPI {
    public static final String SHARED_PREFERENCES_DEFAULT = "default";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String IMAGE = "image";
    public static final String BDAY = "bday";
    public static final String METOD = "authenticationMethod";
    public static final String FIRST_PRESENT_ID = "FP_id";
    public static final String FIRST_PRESENT_IMAGE = "FP_image";
    public static final String FIRST_PRESENT_TITLE = "FP_title";
    public static final String FIRST_PRESENT_DESCRIPTION = "FP_description";

    public static final String DEF = "defaul value";
    private static final String FIRST_PRESENT_PRICE ="price" ;

    public static SharedPreferences getDefaultPreferences() {
        return MainActivity.activity.getSharedPreferences(SHARED_PREFERENCES_DEFAULT, Context.MODE_PRIVATE);
    }


    public static void saveString(String key, String string) {
        SharedPreferences.Editor editor = getDefaultPreferences().edit();
        editor.putString(key, string);
        editor.commit();
    }

    public static void saveInt(String key, int value) {
        SharedPreferences.Editor editor = getDefaultPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static String readString(String key) {
        return getDefaultPreferences().getString(key,DEF);
    }

    public static int readInt(String key) {
        return getDefaultPreferences().getInt(key, 0);
    }

    public static int getId()
    {
        id = getDefaultPreferences().getInt(ID, -1);
        return id;
    }

    public static String getImage()
    {
        image = getDefaultPreferences().getString(IMAGE,DEF);
        return image;
    }
    public static String getName()
    {
        name = getDefaultPreferences().getString(NAME,DEF);
        return name;
    }
    public static String getBDay()
    {
        bday = getDefaultPreferences().getString(BDAY,DEF);
        return bday;
    }
    public static String getAuthenticationMethod()
    {
        authenticationMethod = getDefaultPreferences().getString(METOD,DEF);
        return authenticationMethod;
    }


    private static int id ;
    private static String image = "";
    private static String name = "";
    private static String bday = "";
    private static String authenticationMethod = "";
    public static void saveAuth(int id, String image,String name, String bday,String  authenticationMethod)
    {
        Log.d("qaz",id+image+name+bday+authenticationMethod);
        SharedPreferencesAPI.id = id;
        SharedPreferencesAPI.image = image;
        SharedPreferencesAPI.name = name;
        SharedPreferencesAPI.bday = bday;
        SharedPreferencesAPI.authenticationMethod = authenticationMethod;
        saveInt(ID, id);
        saveString(IMAGE, image);
        saveString(NAME, name);
        saveString(BDAY, bday);
        saveString(METOD, authenticationMethod);
    }

    public static void saveFirstPresent(int FP_id, String FP_image,String FP_title, String FP_description,int FP_price)
    {
        Log.d("qaz",FP_id+FP_image+FP_title+FP_description);
        saveInt(FIRST_PRESENT_ID, FP_id);
        saveInt(FIRST_PRESENT_PRICE, FP_price);
        saveString(FIRST_PRESENT_IMAGE, FP_image);
        saveString(FIRST_PRESENT_TITLE, FP_title);
        saveString(FIRST_PRESENT_DESCRIPTION, FP_description);
    }
    public static Present getFirstPresent()
    {
       Present x = new Present();
       x.setId(readInt(FIRST_PRESENT_ID));
        x.setImagepath( readString(FIRST_PRESENT_IMAGE));
       x.setTitle( readString(FIRST_PRESENT_TITLE));
        x.setDescription(readString(FIRST_PRESENT_DESCRIPTION));
        x.setPrice(readInt(FIRST_PRESENT_PRICE));
        return x;
    }

}
