package zsoft.net.holiday.categories;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import zsoft.net.holiday.App;
import zsoft.net.holiday.R;
import zsoft.net.holiday.goods.GoodsGridActivity;

/**
 * Created by Dimon on 02.07.2015.
 */
public final class CategoryGridActivity extends AppCompatActivity {

    private CategoryGridAdapter adapter;
    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        application = ((App) getApplication());
       // application.setCustomActionBar(this);

        GridView gvCategories = (GridView) findViewById(R.id.gvCategories);
        application.sortBarCategories(false);
        adapter = new CategoryGridAdapter(this, application.getCategories());

        gvCategories.setAdapter(adapter);

        gvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), GoodsGridActivity.class);
                intent.putExtra("id", adapter.getItem(position).getId());
                startActivity(intent);
            }
        });


        final ImageView ivKuhna = (ImageView) findViewById(R.id.ivKuhna);
        final ImageView ivBar = (ImageView) findViewById(R.id.ivBar);
        final ImageView ivKalian = (ImageView) findViewById(R.id.ivKalian);

        findViewById(R.id.btnKuhna).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivKuhna.setImageResource(R.drawable.kuhna_cheked);
                ivBar.setImageResource(R.drawable.bar);
                ivKalian.setImageResource(R.drawable.kalian);
                application.sortBarCategories(false);
                adapter.notifyDataSetChanged();
            }
        });

        findViewById(R.id.btnBar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivKuhna.setImageResource(R.drawable.kuhna);
                ivBar.setImageResource(R.drawable.bar_checked);
                ivKalian.setImageResource(R.drawable.kalian);
                application.sortBarCategories(true);
                adapter.notifyDataSetChanged();
            }
        });

        findViewById(R.id.btnKalian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivKuhna.setImageResource(R.drawable.kuhna);
                ivBar.setImageResource(R.drawable.bar);
                ivKalian.setImageResource(R.drawable.kalian_checked);
            }
        });
    }
}
