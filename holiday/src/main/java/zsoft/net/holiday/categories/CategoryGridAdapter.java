package zsoft.net.holiday.categories;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import zsoft.net.holiday.GrandAdapter;
import zsoft.net.holiday.R;
import zsoft.net.holiday.Server;

public final class CategoryGridAdapter extends GrandAdapter<Category>{

//	private final DisplayImageOptions options;
//	private ImageLoader imageLoader;

	public CategoryGridAdapter(Context mainActivity, List<Category> list) {
		super(mainActivity, list, R.layout.item_category_grid);
//		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mainActivity)
//				.memoryCacheExtraOptions(320, 640) // default = device screen dimensions
//				.diskCacheExtraOptions(320, 640, null)
//				.threadPoolSize(4) // default
//				.build();
//		ImageLoader.getInstance();//.init(config);
//
//		options = new DisplayImageOptions.Builder()
//				.resetViewBeforeLoading(false)  // default
//				.delayBeforeLoading(0)
//				.cacheInMemory(true) // default
//				.cacheOnDisk(true) // default
//				.build();
//
//		imageLoader = ImageLoader.getInstance();

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View view = convertView;
		ViewHolder holder = new ViewHolder();

		if (view == null || convertView.getTag() == null) {
			view = getLayout();
			holder.ivImage = (ImageView) view.findViewById(R.id.ivImage);
			holder.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
			view.setTag(holder);
		}

		holder = (ViewHolder) view.getTag();

		holder.tvTitle.setText(getItem(position).getTitle());

		ContextWrapper cw = new ContextWrapper(getContext());
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir(Server.CATEGORIES, Context.MODE_PRIVATE);
		// Create imageDir
		File mypath=new File(directory,getItem(position).getImagepath());
		Log.d("Cat", mypath.getAbsolutePath());
		Picasso.with(getContext()).load(mypath).into(holder.ivImage);

		// imageLoader.displayImage("file://"+mypath, holder.ivImage, options);
		
		return view;
	}

	static class ViewHolder {
		TextView tvTitle;
		ImageView ivImage;
	}

}