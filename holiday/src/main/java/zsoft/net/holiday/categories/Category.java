package zsoft.net.holiday.categories;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Dimon on 02.07.2015.
 */
@DatabaseTable(tableName = "category")
public final class Category implements Serializable {
    @DatabaseField(id = true,dataType = DataType.INTEGER)
    private int id;
    @DatabaseField( dataType = DataType.STRING)
    private String title;
    @DatabaseField( dataType = DataType.STRING)
    private String imagepath;
    @DatabaseField( dataType = DataType.BOOLEAN)
    private boolean isbar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public boolean isbar() {
        return isbar;
    }

    public void setIsbar(boolean isbar) {
        this.isbar = isbar;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", imagepath='" + imagepath + '\'' +
                ", isbar=" + isbar +
                '}';
    }
}
