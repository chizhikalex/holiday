package zsoft.net.holiday.ORM;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class GoalDAO extends BaseDaoImpl<Goal, String> {

    protected GoalDAO(ConnectionSource connectionSource,
                      Class<Goal> dataClass) throws SQLException{
        super(connectionSource, dataClass);
    }

    public List<Goal> getAllGoals() throws SQLException {

        return this.queryForAll();

    }
    public List<Goal> getGoalByName(String name)  throws SQLException{
        QueryBuilder<Goal, String> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Goal.GOAL_NAME_FIELD_NAME, "First goal");
        PreparedQuery<Goal> preparedQuery = queryBuilder.prepare();
        List<Goal> goalList =query(preparedQuery);
        return goalList;
    }

}