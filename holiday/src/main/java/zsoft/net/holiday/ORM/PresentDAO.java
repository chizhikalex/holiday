package zsoft.net.holiday.ORM;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.present.Present;

/**
 * Created by Sergey on 9/14/15.
 */
public class PresentDAO extends BaseDaoImpl<Present, Integer> {

    protected PresentDAO(ConnectionSource connectionSource,
                         Class<Present> dataClass) throws SQLException {
        super(connectionSource, dataClass);

    }

    public List<Present> getAllPresents() throws SQLException {

        return this.queryForAll();

    }

    public Present getPresentById(Integer id)  throws SQLException{
        QueryBuilder<Present, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("id", id);
        PreparedQuery<Present> preparedQuery = queryBuilder.prepare();
        ArrayList<Present> goalList =(ArrayList)query(preparedQuery);

        return goalList.get(0) ;
    }

}
