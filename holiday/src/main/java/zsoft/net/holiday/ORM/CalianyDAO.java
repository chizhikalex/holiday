package zsoft.net.holiday.ORM;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.Caliany.Calian;

/**
 * Created by Sergey on 9/18/15.
 */
public class CalianyDAO extends BaseDaoImpl<Calian, Integer> {

    protected CalianyDAO(ConnectionSource connectionSource,
                       Class<Calian> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Calian> getAllCalians() throws SQLException {

        return this.queryForAll();

    }



    public Calian getCalianById(Integer id)  throws SQLException{
        QueryBuilder<Calian, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("id", id);
        PreparedQuery<Calian> preparedQuery = queryBuilder.prepare();
        ArrayList<Calian> goalList =(ArrayList)query(preparedQuery);

        return goalList.get(0) ;
    }
}