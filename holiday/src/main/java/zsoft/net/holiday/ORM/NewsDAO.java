package zsoft.net.holiday.ORM;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.news.News;

/**
 * Created by Sergey on 9/14/15.
 */
public class NewsDAO extends BaseDaoImpl<News, Integer> {

    protected NewsDAO(ConnectionSource connectionSource,
                      Class<News> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<News> getAllNews() throws SQLException {

        return this.queryForAll();

    }

    public News getNewsById(Integer id)  throws SQLException{
        QueryBuilder<News, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("id", id);
        PreparedQuery<News> preparedQuery = queryBuilder.prepare();
       ArrayList<News> goalList =(ArrayList)query(preparedQuery);

        return goalList.get(0) ;
    }

}
