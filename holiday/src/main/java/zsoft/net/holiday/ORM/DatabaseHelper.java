package zsoft.net.holiday.ORM;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import zsoft.net.holiday.Caliany.Calian;
import zsoft.net.holiday.categories.Category;
import zsoft.net.holiday.goods.Good;
import zsoft.net.holiday.news.News;
import zsoft.net.holiday.present.Present;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static final String DATABASE_NAME ="myappname.db";

    //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();
    private static final int DATABASE_VERSION = 1;

    //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private GoalDAO goalDao = null;
    private NewsDAO newsDao = null;
    private PresentDAO presentDao = null;
    private CategoryDAO categoryDao = null;
    private GoodsDAO goodsDao = null;
    private CalianyDAO calianyDao = null;
    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Выполняется, когда файл с БД не найден на устройстве
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try
        {
            TableUtils.createTable(connectionSource, Goal.class);
            TableUtils.createTable(connectionSource, Present.class);
            TableUtils.createTable(connectionSource, News.class);
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Good.class);
            TableUtils.createTable(connectionSource, Calian.class);
        }
        catch (SQLException e){
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    //Выполняется, когда БД имеет версию отличную от текущей
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer){
        try{
            //Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
            TableUtils.dropTable(connectionSource, Goal.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e){
            Log.e(TAG,"error upgrading db "+DATABASE_NAME+"from ver "+oldVer);
            throw new RuntimeException(e);
        }
    }

    //синглтон для GoalDAO
    public GoalDAO getGoalDAO() throws SQLException{
        if(goalDao == null){
            goalDao = new GoalDAO(getConnectionSource(), Goal.class);
        }
        return goalDao;
    }

    public GoodsDAO getGoodsDAO() throws SQLException{
        if(goodsDao == null){
            goodsDao = new GoodsDAO(getConnectionSource(), Good.class);
        }
        return goodsDao;
    }

    public NewsDAO getNewsDAO() throws SQLException{
        if(newsDao == null){
            newsDao = new NewsDAO(getConnectionSource(), News.class);
        }
        return newsDao;
    }

    public PresentDAO getPresentDAO() throws SQLException{
        if(presentDao == null){
            presentDao = new PresentDAO(getConnectionSource(), Present.class);
        }
        Log.d("cr db","person");
        return presentDao;
    }

    public CategoryDAO getCategoryDAO() throws SQLException{
        if(categoryDao == null){
            categoryDao = new CategoryDAO(getConnectionSource(), Category.class);
        }
        return categoryDao;
    }

    public CalianyDAO getCalianyDAO() throws SQLException{
        if(calianyDao == null){
            calianyDao = new CalianyDAO(getConnectionSource(), Calian.class);
        }
        return calianyDao;
    }

    //выполняется при закрытии приложения

    @Override
    public void close(){
        super.close();
        goalDao = null;
        newsDao = null;
        presentDao = null;
        categoryDao = null;
        goodsDao = null;
        calianyDao=null;
    }
}
