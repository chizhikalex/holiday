package zsoft.net.holiday.ORM;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.categories.Category;

/**
 * Created by Sergey on 9/14/15.
 */
public class CategoryDAO extends BaseDaoImpl<Category, Integer> {
    protected CategoryDAO(ConnectionSource connectionSource,
                      Class<Category> dataClass) throws SQLException{
        super(connectionSource, dataClass);
    }

    public List<Category> getAllCategory() throws SQLException {

        return this.queryForAll();

    }


    public List<Category> getAllBar() throws SQLException {
        ArrayList<Category> categories= new ArrayList<>();

        for (Category c :this.queryForAll()
             ) {
            if(c.isbar()){
                categories.add(c);
            }


        }



        return categories;

    }

    public List<Category> getAllKitchen() throws SQLException {

        ArrayList<Category> categories= new ArrayList<>();

        for (Category c :this.queryForAll()
                ) {
            if(!c.isbar()){
                categories.add(c);
            }

        }

        return categories;

    }

    public Category getCategoryById(Integer id)  throws SQLException{
        QueryBuilder<Category, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("id", id);
        PreparedQuery<Category> preparedQuery = queryBuilder.prepare();
        ArrayList<Category> goalList =(ArrayList)query(preparedQuery);

        return goalList.get(0) ;
    }

}
