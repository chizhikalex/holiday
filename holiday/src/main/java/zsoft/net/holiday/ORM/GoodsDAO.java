package zsoft.net.holiday.ORM;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.goods.Good;


public class GoodsDAO extends BaseDaoImpl<Good, Integer> {

    protected GoodsDAO(ConnectionSource connectionSource,
                       Class<Good> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Good> getAllGoods() throws SQLException {

        return this.queryForAll();

    }

    public List<Good> getGoodsByCategoryId(int id) throws SQLException {
        ArrayList<Good> temp = new ArrayList<>();
        for (Good good:this.queryForAll()
             ) {
            if(good.getCategoriesid()==id){
                temp.add(good);
            }
        }


        return temp;

    }

    public Good getGoodById(Integer id)  throws SQLException{
        QueryBuilder<Good, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("id", id);
        PreparedQuery<Good> preparedQuery = queryBuilder.prepare();
        ArrayList<Good> goalList =(ArrayList)query(preparedQuery);

        return goalList.get(0) ;
    }


}