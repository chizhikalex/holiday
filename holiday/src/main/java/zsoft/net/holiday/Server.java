package zsoft.net.holiday;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import zsoft.net.holiday.Caliany.Calian;
import zsoft.net.holiday.ORM.HelperFactory;
import zsoft.net.holiday.categories.Category;
import zsoft.net.holiday.goods.Good;
import zsoft.net.holiday.news.News;
import zsoft.net.holiday.present.Present;

/**
 * Created by Dimon on 01.07.2015.
 */
public final class Server {

    public static final String URL = "http://lali.com.ua/";
    public static final String PRESENT_SCRIPT = "presents_get_all.php";
    public static final String NEWS_SCRIPT = "news_get_all.php";
    public static final String CATEGORY_SCRIPT = "category_get_all.php";
    public static final String CALIANY_SCRIPT = "caliany_get_all.php";
    public static final String GOODS_SCRIPT = "goods_get_all.php";
    public static final String FIRST_PRESENT_SCRIPT = "first_present_get.php";
    public static final File DIR = new File(MainActivity.activity.getFilesDir().getAbsolutePath() + "/museum/gallery/");
    public static final String PRESENTS = "presents";
    public static final String NEWS = "news";
    public static final String CATEGORIES = "categories";
    public static final String FIRST_PRESENT = "firstpresent";
    public static final String GOODS = "goods";
    public static final String CALIANY = "caliany";
    public static JSONArray firstPresentsArray = new JSONArray();
    public static JSONArray calianyArray = new JSONArray();
    public static JSONArray presentsArray = new JSONArray();
    public static JSONArray newsArray = new JSONArray();
    public static JSONArray categoriesArray = new JSONArray();
    public static JSONArray goodsArray = new JSONArray();

    public Server() {
        if (!DIR.exists()) {
            DIR.mkdirs();
        }
    }

    public static Present getFirstPresents() {


        try {
            JSONArray array = firstPresentsArray; //getJsonArray(FIRST_PRESENT_SCRIPT, FIRST_PRESENT);


            JSONObject json = array.getJSONObject(0);
            Log.d("", json.toString());


//                present.setId(json.getInt("id"));
//                present.setTitle(json.getString("title"));
//                present.setDescription(json.getString("description"));
//                present.setImagepath(productLookup(PRESENTS,json.getString("imagepath"), MainActivity.activity));
//               // present.setImagepath(json.getString("imagepath"));
//                present.setPrice(json.getInt("price"));
            if (SharedPreferencesAPI.readString(SharedPreferencesAPI.FIRST_PRESENT_IMAGE) != null) {

                ContextWrapper cw = new ContextWrapper(MainActivity.activity);
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir(Server.PRESENTS, Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, json.getString("imagepath"));
                mypath.delete();
            }

            SharedPreferencesAPI.saveFirstPresent(json.getInt("id"), productLookup(PRESENTS, json.getString("imagepath"), MainActivity.activity), json.getString("title"), json.getString("description"), json.getInt("price"));


        } catch (JSONException e) {
            Log.d("HttpPost", e.getMessage());
        }

        return SharedPreferencesAPI.getFirstPresent();
    }


    public static List<Calian> getCaliany() {
        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<Calian> gooads = new ArrayList<Calian>();

        try {
            JSONArray array = calianyArray;// getJsonArray(CALIANY_SCRIPT, CALIANY);
            gooads = (ArrayList) HelperFactory.getHelper().getCalianyDAO().getAllCalians();
            m:
            for (int index = 0; index < array.length(); ++index) {
                JSONObject json = array.getJSONObject(index);
                Log.d("caliany", json.toString());
                int id = json.getInt("id");
                integers.add(id);
                String title = json.getString("title");
                String description = json.getString("description");
                String imagepath = json.getString("imagepath");
                String moredescription = json.getString("moredescription");
                int price = json.getInt("price");

                p:
                for (Calian g : gooads
                        ) {
                    if (id == g.getId()) {
                        if (title.equals(g.getTitle()) && description.equals(g.getDescription()) && imagepath.equals(g.getImagepath()) && price == g.getPrice() && moredescription.equals(g.getMoredescription())) {
                            continue m;
                        } else {
                            HelperFactory.getHelper().getGoodsDAO().deleteById(id);
                            continue p;
                        }
                    }
                }
                Calian calian = new Calian();

                calian.setId(id);
                calian.setTitle(title);
                calian.setDescription(description);
                calian.setMoredescription(moredescription);

                calian.setPrice(price);

                //good.setImagepath(json.getString("imagepath"));
                Log.d("parse ex", imagepath);

                String path = productLookup(CALIANY, imagepath, MainActivity.activity);
                if (path.equals("exeption")) {
                    continue m;
                } else {
                    calian.setImagepath(path);
                }
                try {
                    HelperFactory.getHelper().getCalianyDAO().create(calian);

                    Log.d("parse ex", "added");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            Log.d("HttpPost", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        s:
        for (Calian i : gooads) {
            for (Integer y : integers) {
                if (y.equals(i.getId())) {
                    continue s;
                }
            }
            try {
                ContextWrapper cw = new ContextWrapper(MainActivity.activity);
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir(Server.CALIANY, Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, HelperFactory.getHelper().getCalianyDAO().getCalianById(i.getId()).getImagepath());
                mypath.delete();
                HelperFactory.getHelper().getCalianyDAO().deleteById(i.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return gooads;
    }


    public static ArrayList<Present> getPresents() {

        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<Present> gooads = null;
        try {
            gooads = (ArrayList) HelperFactory.getHelper().getPresentDAO().getAllPresents();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Present present;
        try {
            JSONArray array = presentsArray;//getJsonArray(PRESENT_SCRIPT, PRESENTS);


            JSONObject json;
            w:
            for (int index = 0; index < array.length(); ++index) {
                json = array.getJSONObject(index);


                int id = json.getInt("id");
                integers.add(id);
                String title = json.getString("title");
                String description = json.getString("description");
                String imagepath = json.getString("imagepath");
                int price = json.getInt("price");
                p:
                for (Present g : gooads
                        ) {
                    if (id == g.getId()) {
                        if (title.equals(g.getTitle()) && description.equals(g.getDescription()) && imagepath.equals(g.getImagepath()) && price == g.getPrice()) {
                            continue w;
                        } else {
                            HelperFactory.getHelper().getPresentDAO().deleteById(id);
                            continue p;
                        }
                    }
                }

                present = new Present();

                present.setId(id);
                present.setTitle(title);
                present.setDescription(description);
                //present.setImagepath(json.getString("imagepath"));
                String path = productLookup(PRESENTS, imagepath, MainActivity.activity);
                if (path.equals("exeption")) {
                    continue w;
                } else {
                    present.setImagepath(path);
                }
                //present.setImagepath(productLookup(PRESENTS,imagepath, MainActivity.activity));
                present.setPrice(price);

                //presents.add(present);

                HelperFactory.getHelper().getPresentDAO().create(present);
            }

        } catch (JSONException e) {
            Log.d("HttpPost", e.getMessage());
        } catch (SQLException e) {
            Log.d("HttpPost", e.getMessage());
            e.printStackTrace();
            //throw e;
        }
        s:
        for (Present i : gooads
                ) {

            if(integers.size() == 0){
                break;
            }

            for (Integer y : integers
                    ) {
                if (y.equals(i.getId())) {

                    continue s;
                }
            }
            try {
                ContextWrapper cw = new ContextWrapper(MainActivity.activity);
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir(Server.NEWS, Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, HelperFactory.getHelper().getPresentDAO().getPresentById(i.getId()).getImagepath());
                mypath.delete();
                HelperFactory.getHelper().getPresentDAO().deleteById(i.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return gooads;
    }

    public static ArrayList<News> getNews() {
        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<News> gooads = new ArrayList<News>();
        try {
            JSONArray array = newsArray;//getJsonArray(NEWS_SCRIPT, NEWS);

            e:
            for (int index = 0; index < array.length(); ++index) {
                JSONObject json = array.getJSONObject(index);
                gooads = (ArrayList) HelperFactory.getHelper().getNewsDAO().getAllNews();

                int id = json.getInt("id");
                integers.add(id);
                String title = json.getString("title");
                String description = json.getString("description");
                String imagepath = json.getString("imagepath");
                String when = json.getString("when");
                String fromdate = json.getString("fromdate");
                String todate = json.getString("todate");
                p:
                for (News g : gooads
                        ) {
                    if (id == g.getId()) {
                        if (title.equals(g.getTitle()) && description.equals(g.getDescription()) && imagepath.equals(g.getImagepath()) && when.equals(g.getWhen()) && fromdate.equals(g.getFromdate()) && todate.equals(g.getTodate())) {
                            continue e;
                        } else {
                            HelperFactory.getHelper().getNewsDAO().deleteById(id);
                            continue p;
                        }
                    }
                }
                News neww = new News();

                neww.setId(id);
                neww.setTitle(title);
                neww.setDescription(description);
                //neww.setImagepath(json.getString("imagepath"));
                String path = productLookup(NEWS, imagepath, MainActivity.activity);
                if (path.equals("exeption")) {
                    continue e;
                } else {
                    neww.setImagepath(path);
                }
                //neww.setImagepath(productLookup(NEWS,imagepath, MainActivity.activity));
                neww.setWhen(when);
                neww.setFromdate(fromdate);
                neww.setTodate(todate);

                HelperFactory.getHelper().getNewsDAO().create(neww);
            }

        } catch (JSONException e) {
            Log.d("HttpPost", e.getMessage());
        } catch (SQLException e) {
            Log.d("HttpPost", e.getMessage());
            e.printStackTrace();
        }

        s:
        for (News i : gooads
                ) {

            if(integers.size() == 0){
                break;
            }

            for (Integer y : integers
                    ) {
                if (y.equals(i.getId())) {
                    continue s;
                }
            }
            try {
                ContextWrapper cw = new ContextWrapper(MainActivity.activity);
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir(Server.NEWS, Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, HelperFactory.getHelper().getNewsDAO().getNewsById(i.getId()).getImagepath());
                mypath.delete();
                HelperFactory.getHelper().getNewsDAO().deleteById(i.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return gooads;
    }

    public static ArrayList<Category> getCategories() {
        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<Category> gooads = new ArrayList<>();
        try {
            JSONArray array = categoriesArray;//getJsonArray(CATEGORY_SCRIPT, CATEGORIES);
            gooads = (ArrayList) HelperFactory.getHelper().getCategoryDAO().getAllCategory();
            Log.d("CATEGORY", gooads.toString());


            q:
            for (int index = 0; index < array.length(); ++index) {
                JSONObject json = array.getJSONObject(index);

                int id = json.getInt("id");
                integers.add(id);
                String title = json.getString("title");
                String imagepath = json.getString("imagepath");
                int isbar = json.getInt("isbar");
                r:
                for (Category g : gooads
                        ) {
                    if (id == g.getId()) {
                        if (title.equals(g.getTitle()) && imagepath.equals(g.getImagepath()) && ((isbar == 1) ? true : false) == g.isbar()) {
                            continue q;
                        } else {
                            HelperFactory.getHelper().getCategoryDAO().deleteById(id);
                            Log.d("Catgory", "id is = " + id + " deleting");
                            continue r;
                        }
                    }
                }
                Category category = new Category();

                category.setId(id);
                category.setTitle(title);
                //category.setImagepath(json.getString("imagepath"));
                Log.d("parse ex", imagepath);
                String path = productLookup(CATEGORIES, imagepath, MainActivity.activity);
                if (path.equals("exeption")) {
                    continue q;
                } else {
                    category.setImagepath(path);
                }
                //category.setImagepath(productLookup(CATEGORIES,imagepath, MainActivity.activity));
                category.setIsbar((isbar == 1) ? true : false);

                try {
                    Log.d("Cat", category.toString());
                    HelperFactory.getHelper().getCategoryDAO().create(category);
                } catch (SQLException e) {
                    Log.d("HttpPost", e.getMessage());
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            Log.d("HttpPost", e.getMessage());
        } catch (SQLException e) {
            Log.d("HttpPost", e.getMessage());
            e.printStackTrace();
        }



        s:
        for (Category i : gooads) {

            if(integers.size() == 0){
                break;
            }

            for (Integer y : integers
                    ) {
                if (y.equals(i.getId())) {
                    continue s;
                }
            }
            try {
                ContextWrapper cw = new ContextWrapper(MainActivity.activity);
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir(Server.CATEGORIES, Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, HelperFactory.getHelper().getCategoryDAO().getCategoryById(i.getId()).getImagepath());//todo
                mypath.delete();
                HelperFactory.getHelper().getCategoryDAO().deleteById(i.getId());
                Log.d("Catgory", "id is = " + i.getId() + " deleting");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return gooads;
    }

    public static JSONArray getJsonArray(String script, String arrName) throws IOException, JSONException {
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(URL + script);
        HttpResponse response = client.execute(request);

        String result = EntityUtils.toString(response.getEntity());
        JSONObject mainObject = new JSONObject(result);
        return mainObject.getJSONArray(arrName);
    }


    public static List<Good> getGoods() {
        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<Good> gooads = new ArrayList<Good>();

        try {
            JSONArray array = goodsArray;//getJsonArray(GOODS_SCRIPT, GOODS);
            gooads = (ArrayList) HelperFactory.getHelper().getGoodsDAO().getAllGoods();
            m:
            for (int index = 0; index < array.length(); ++index) {
                JSONObject json = array.getJSONObject(index);
                Log.d("good", json.toString());
                int id = json.getInt("id");
                integers.add(id);
                String title = json.getString("title");
                String description = json.getString("description");
                String imagepath = json.getString("imagepath");
                String moredescription = json.getString("moredescription");
                int categoriesid = json.getInt("categoriesid");
                int price = json.getInt("price");

                p:
                for (Good g : gooads
                        ) {
                    if (id == g.getId()) {
                        if (title.equals(g.getTitle()) && description.equals(g.getDescription()) && imagepath.equals(g.getImagepath()) && price == g.getPrice() && moredescription.equals(g.getMoredescription()) && categoriesid == g.getCategoriesid()) {
                            continue m;
                        } else {
                            HelperFactory.getHelper().getGoodsDAO().deleteById(id);
                            continue p;
                        }
                    }
                }
                Good good = new Good();

                good.setId(id);
                good.setTitle(title);
                good.setDescription(description);
                good.setMoredescription(moredescription);
                good.setCategoriesid(categoriesid);

                good.setPrice(price);

                //good.setImagepath(json.getString("imagepath"));
                Log.d("parse ex", imagepath);

                String path = productLookup(GOODS, imagepath, MainActivity.activity);
                if (path.equals("exeption")) {
                    continue m;
                } else {
                    good.setImagepath(path);
                }
                try {
                    HelperFactory.getHelper().getGoodsDAO().create(good);

                    Log.d("parse ex", "added");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            Log.d("HttpPost", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        s:
        for (Good i : gooads
                ) {

            if(integers.size() == 0){
                break;
            }

            for (Integer y : integers
                    ) {
                if (y.equals(i.getId())) {
                    continue s;
                }
            }
            try {
                ContextWrapper cw = new ContextWrapper(MainActivity.activity);
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir(Server.GOODS, Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, HelperFactory.getHelper().getGoodsDAO().getGoodById(i.getId()).getImagepath());//todo
                mypath.delete();
                HelperFactory.getHelper().getGoodsDAO().deleteById(i.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return gooads;
    }

    public static String productLookup(String category, String urlx, Context c) {
        InputStream input = null;
        FileOutputStream output = null;
        try {
            Log.d("parse ex", "in");
            URL url = new URL(URL + "img/" + urlx);

            //urlx = urlx.replace("http://holiday.pe.hu/img/", "");

            input = url.openConnection().getInputStream();
            //output= new FileOutputStream(DIR.getAbsolutePath()+"/"+category+"/"+urlx);
            //File file = new File(DIR.getAbsolutePath()+"/"+category+"/"+urlx);what the fuck,
//            File file = new File(DIR.getAbsolutePath()+"/"+category+"/"+urlx);
//            file.createNewFile();


            ContextWrapper cw = new ContextWrapper(c);
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir(category, Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, urlx);
            output = new FileOutputStream(mypath);
            //output= MainActivity.activity. openFileOutput(category+urlx, Context.MODE_PRIVATE);
            int read;
            byte[] data = new byte[1024];
            while ((read = input.read(data)) != -1) {
                output.write(data, 0, read);
            }


        } catch (Throwable e) {
            e.printStackTrace();
            Log.d("parse ex", e.getMessage());
            urlx = "exeption";


        } finally {

            if (output != null)
                try {
                    output.flush();
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("parse ex", e.toString());
                }
            if (input != null)
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("parse ex", e.toString());
                }
            Log.d("parse ex", urlx.toString());
            return urlx;
        }

    }


}
