package zsoft.net.holiday.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;

import zsoft.net.holiday.ORM.HelperFactory;
import zsoft.net.holiday.R;
import zsoft.net.holiday.news.News;
import zsoft.net.holiday.news.NewsGridAdapter;


public class NewsGridFragment extends Fragment {
    private View view;
    private NewsGridAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
        view = inflater.inflate(R.layout.activity_news,null);

        ImageView back = (ImageView) view.findViewById(R.id.back);

        ImageLoader.getInstance().displayImage("drawable://" + R.drawable.back, back);


//        Picasso.with(getActivity()).load(R.drawable.back).into((ImageView) view.findViewById(R.id.back));

        GridView gvNews = (GridView)view.findViewById(R.id.gvNews);
       ArrayList<News> news = new ArrayList<>();
        try {
            news= (ArrayList) HelperFactory.getHelper().getNewsDAO().getAllNews();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        adapter = new NewsGridAdapter(getActivity(), news);
        gvNews.setAdapter(adapter);

        gvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new NewsDescriptionFragment().setNews(adapter.getItem(position))).addToBackStack(null).commit();
                    }
                }, 250);


            }
        });

        return view;
    }
}
