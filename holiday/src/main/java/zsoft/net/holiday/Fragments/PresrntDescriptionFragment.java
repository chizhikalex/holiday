package zsoft.net.holiday.Fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.Serializable;

import zsoft.net.holiday.App;
import zsoft.net.holiday.InfoFragment;
import zsoft.net.holiday.MainActivity;
import zsoft.net.holiday.R;
import zsoft.net.holiday.Server;
import zsoft.net.holiday.present.Present;

/**
 * Created by Sergey on 9/8/15.
 */
public class PresrntDescriptionFragment extends Fragment {
    private View view;
    private App application;
    public final static int WHITE = 0xFFFFFFFF;
    public final static int BLACK = 0xFF000000;
    public final static int WIDTH = 400;
    public final static int HEIGHT = 400;

    public Serializable getPresent() {
        return present;
    }

    public PresrntDescriptionFragment setPresent(Serializable present) {
        this.present = present;
        return this;
    }

    private Serializable present;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_present_description, null);


        view.findViewById(R.id.btn_get).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                View dialog = getActivity().getLayoutInflater().inflate(R.layout.present_dialog, null);
//
//                final Dialog callDialog = new Dialog(getActivity());
//                callDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                callDialog.setContentView(dialog);
//                callDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                callDialog.show();
//                dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
//                                                                           @Override
//                                                                           public void onClick(View view) {
//                                                                               callDialog.cancel();
//                                                                           }
//                                                                       });
//
//                        ImageView imageView = (ImageView) view.findViewById(R.id.qr_code);
//                try {
//                    Bitmap bitmap = encodeAsBitmap(((Present) present).getId() + "");
//                    imageView.setImageBitmap(bitmap);
//                } catch (WriterException e) {
//                    e.printStackTrace();
//                }
//                ((Button)view.findViewById(R.id.btn_get)).setText("ГОТОВО");
//                view.findViewById(R.id.btn_get).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        FragmentManager fragmentManager =  getActivity().getFragmentManager();
//                        if (fragmentManager.getBackStackEntryCount() > 0) {
//                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        }
//                    }
//                });
//                view.findViewById(R.id.qr_code).setVisibility(View.VISIBLE);
//
//            }
//        });


                View dialog = getActivity().getLayoutInflater().inflate(R.layout.private_qr, null);

                final Dialog callDialog = new Dialog(getActivity());
                callDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                callDialog.setContentView(dialog);
                callDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                callDialog.show();
                dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callDialog.cancel();
                    }
                });
                ((TextView) dialog.findViewById(R.id.description)).setText("Покажите это официанту и он принесет вам подарок!");
                ImageView imageView = (ImageView) dialog.findViewById(R.id.private_qr);
                try {
                    Bitmap bitmap = PresrntDescriptionFragment.encodeAsBitmap(((Present) present).getId() + "");
                    imageView.setImageBitmap(bitmap);
                } catch (WriterException e) {
                    e.printStackTrace();
                }
//                ((Button)view.findViewById(R.id.btn_get)).setText("ГОТОВО");
//                view.findViewById(R.id.btn_get).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        FragmentManager fragmentManager =  getActivity().getFragmentManager();
//                        if (fragmentManager.getBackStackEntryCount() > 0) {
//                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        }
//                    }
//                });
                //view.findViewById(R.id.qr_code).setVisibility(View.VISIBLE);

            }
        });

        view.findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getFragmentManager().popBackStack();
            }
        });

        application = ((App) getActivity().getApplication());
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            application.colorizeInfoBar(getActivity());
        }

        // Present present = (Present) getIntent().getSerializableExtra("present");
        Present present = (Present) getPresent();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .memoryCacheExtraOptions(320, 640) // default = device screen dimensions
                .diskCacheExtraOptions(320, 640, null)
                .threadPoolSize(4) // default
                .build();
        ImageLoader.getInstance();//;//.init(config);
        ImageLoader imageLoader = ImageLoader.getInstance();


        ImageView ivImage = (ImageView) view.findViewById(R.id.ivImage);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);

       // imageLoader.displayImage(present.getImagepath(), ivImage);

        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir(Server.PRESENTS, Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,present.getImagepath());
        Picasso.with(getActivity()).load(mypath).into(ivImage);
        //imageLoader.displayImage("file://"+mypath, ivImage);

        tvPrice.setText(present.getPrice() + "");
        tvTitle.setText(present.getTitle());
        tvDescription.setText(present.getDescription());


        view.findViewById(R.id.infoBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).mDrawerLayout.openDrawer(Gravity.RIGHT);

            }
        });
        settingInfoFragment();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        //menu.setSlidingEnabled(true);
        MainActivity.isDescriptionScreen = true;
        ((MainActivity) getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.RIGHT);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }


    @Override
    public void onStop() {
        super.onStop();
//        if(menu.isMenuShowing()){
//            menu.toggle();
//        }
//        menu.setSlidingEnabled(false);
        MainActivity.isDescriptionScreen = false;
        ((MainActivity) getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    void settingInfoFragment() {
        ((TextView) InfoFragment.view.findViewById(R.id.textView25)).setText(((Present) present).getDescription());
//        if(((Present)present).get()==null||moredescription.length()==0){
        InfoFragment.view.findViewById(R.id.textView26).setVisibility(View.GONE);
        InfoFragment.view.findViewById(R.id.textView27).setVisibility(View.GONE);
//        }else{
//            ((TextView)  view.findViewById(R.id.textView27)).setText(moredescription);
//        }
    }

    static Bitmap encodeAsBitmap(String str) throws com.google.zxing.WriterException {
        com.google.zxing.common.BitMatrix result;
        try {
            result = new com.google.zxing.MultiFormatWriter().encode(str,
                    com.google.zxing.BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        return bitmap;
    }


}
