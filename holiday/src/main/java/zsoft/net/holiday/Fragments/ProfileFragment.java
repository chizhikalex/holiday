package zsoft.net.holiday.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;

import zsoft.net.holiday.MainActivity;
import zsoft.net.holiday.MenuListFragment;
import zsoft.net.holiday.R;

public class ProfileFragment extends Fragment {

    private View view;
    private MainActivity activity;
    private int myYear = 2011;
    private int myMonth = 2;
    private int myDay = 3;
    private DatePickerDialog pickerDialog;
    private EditText data;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity= (MainActivity)activity;
        pickerDialog = new DatePickerDialog(activity, myCallBack, myYear, myMonth, myDay);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(view !=null){
            return view;
        }

        view = inflater.inflate(R.layout.profile_fragment,null);
        data =(EditText)  view.findViewById(R.id.data_edit_text);
        view.findViewById(R.id.data_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    pickerDialog.show();
            }
        });

        return view;
    }



    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;
            data.setText(myDay + "." + myMonth + "." + myYear);
            //tvDate.setText("Today is " + myDay + "/" + myMonth + "/" + myYear);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        MenuListFragment.setLastMenuItem(MenuListFragment.MENU_SETTINGS);
    }

    @Override
    public void onPause() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        super.onPause();
    }
}
