package zsoft.net.holiday.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.List;

import zsoft.net.holiday.ORM.HelperFactory;
import zsoft.net.holiday.R;
import zsoft.net.holiday.present.Present;
import zsoft.net.holiday.present.PresentsGridAdapter;

/**
 * Created by Sergey on 9/8/15.
 */
public class PresentsGridFragment extends Fragment {
    private View view;
    private PresentsGridAdapter adapter;
    List<Present> presents;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
        view = inflater.inflate(R.layout.activity_presents,null);

        ImageView back = (ImageView) view.findViewById(R.id.back);

        ImageLoader.getInstance().displayImage("drawable://" + R.drawable.back, back);


//        Picasso.with(getActivity()).load(R.drawable.back).into((ImageView) view.findViewById(R.id.back));

        try {
            presents=  HelperFactory.getHelper().getPresentDAO().getAllPresents();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        GridView gvPresents = (GridView) view.findViewById(R.id.gvPresents);
        adapter = new PresentsGridAdapter(getActivity(), presents);
        gvPresents.setAdapter(adapter);

        gvPresents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                Intent intent = new Intent(getActivity(), PresentsDescriptionActivity.class);
//                intent.putExtra("present", adapter.getItem(position));
//                startActivity(intent);

                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame,
                        new PresrntDescriptionFragment().setPresent(adapter.getItem(position))).addToBackStack(null).commit();


            }
        });

        return view;
    }
}
