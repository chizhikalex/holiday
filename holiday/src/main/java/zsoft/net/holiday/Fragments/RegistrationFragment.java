package zsoft.net.holiday.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import zsoft.net.holiday.MainActivity;
import zsoft.net.holiday.MenuListFragment;
import zsoft.net.holiday.R;


public class RegistrationFragment extends Fragment {
    MainActivity activity;
    View view;
    int myYear = 2011;
    int myMonth = 02;
    int myDay = 03;
    DatePickerDialog pickerDialog;
    EditText data;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity= (MainActivity)activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
         pickerDialog = new DatePickerDialog(activity, myCallBack, myYear, myMonth, myDay);
        view =  inflater.inflate(R.layout.registration_fragment,null);

        data =(EditText)  view.findViewById(R.id.data_edit_text);
        view.findViewById(R.id.data_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickerDialog.show();
            }
        });
        final EditText ev =((EditText) view.findViewById(R.id.name));
        view.findViewById(R.id.create_accaut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("xp", (MenuListFragment.view.findViewById(R.id.textView)==null)+"");
                ((TextView)MenuListFragment.view.findViewById(R.id.textView)).setText(ev
                        .getText().toString());
                getFragmentManager().popBackStack();
            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(MainFragment.loginDialog!=null)
        MainFragment.loginDialog.dismiss();
    }


    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;
            data.setText(myDay + "." + myMonth + "." + myYear);
            //tvDate.setText("Today is " + myDay + "/" + myMonth + "/" + myYear);
        }
    };
}
