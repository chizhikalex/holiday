package zsoft.net.holiday.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import zsoft.net.holiday.Caliany.Calian;
import zsoft.net.holiday.MainActivity;
import zsoft.net.holiday.R;
import zsoft.net.holiday.Server;
import zsoft.net.holiday.categories.Category;
import zsoft.net.holiday.goods.Good;
import zsoft.net.holiday.news.News;
import zsoft.net.holiday.present.Present;

/**
 * Created by Sergey on 9/15/15.
 */
public class LoadingFragment extends Fragment {
    MainActivity activity;
    View view;
    int countOfFineshedAsyncTask=0;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity =(MainActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }


        //at first get all JSON
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    Server.firstPresentsArray =Server.getJsonArray(Server.FIRST_PRESENT_SCRIPT, Server.FIRST_PRESENT);
//                    Server.presentsArray =Server.getJsonArray(Server.PRESENT_SCRIPT, Server.PRESENTS);
                    Server.newsArray =Server.getJsonArray(Server.NEWS_SCRIPT, Server.NEWS);
                    Server.calianyArray =Server.getJsonArray(Server.CALIANY_SCRIPT, Server.CALIANY);
                    Server.categoriesArray =Server.getJsonArray(Server.CATEGORY_SCRIPT, Server.CATEGORIES);
                    Server.goodsArray =Server.getJsonArray(Server.GOODS_SCRIPT, Server.GOODS);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
               // Server.getCaliany();
                return null;
            }
            @Override
            protected void onPostExecute(Void x) {

                new AsyncTask<Void, Void, List<Calian>>() {
                    @Override
                    protected List<Calian> doInBackground(Void... voids) {
                        return Server.getCaliany();
                    }
                    @Override
                    protected void onPostExecute(List<Calian> presents) {
                        // ((MainActivity)getActivity()).
                        lodiangProgres();
                        Log.d("lodiang","Calian finished");
                    }
                }.execute();


                new AsyncTask<Void, Void, Present> () {
                    @Override
                    protected Present doInBackground(Void... voids) {
                        return Server.getFirstPresents();
                    }
                    @Override
                    protected void onPostExecute(Present firstPresent) {

                        // ((MainActivity)getActivity()).
                        activity.application
                                .setFirstPresents(firstPresent);

                        lodiangProgres();
                        //Log.d("lodiang",firstPresent.toString());
                    }
                }.execute();


//                new AsyncTask<Void, Void, List<Present>>() {
//                    @Override
//                    protected List<Present> doInBackground(Void... voids) {
//                        return Server.getPresents();
//                    }
//                    @Override
//                    protected void onPostExecute(List<Present> presents) {
//                        // ((MainActivity)getActivity()). or some of usNaN
//                        activity.application.setPresents(presents);
//                        lodiangProgres();
//                        Log.d("lodiang","Present finished");
//                    }
//                }.execute();


                new AsyncTask<Void, Void, List<Category>> () {
                    @Override
                    protected List<Category> doInBackground(Void... voids) {
                        return Server.getCategories();
                    }
                    @Override
                    protected void onPostExecute(List<Category> categories) {
                        // ((MainActivity)getActivity()).
                        activity.application.setCategories(categories);
                        lodiangProgres();
                        Log.d("lodiang","Category finished");
                    }
                }.execute();

                new AsyncTask<Void, Void, List<Good>> () {
                    @Override
                    protected List<Good> doInBackground(Void... voids) {
                        return Server.getGoods();
                    }
                    @Override
                    protected void onPostExecute(List<Good> goods) {
                        // ((MainActivity)getActivity()).
                        activity.application.setGoods(goods);
                        lodiangProgres();
                        Log.d("lodiang","Good finished");
                    }
                }.execute();


                new AsyncTask<Void, Void, List<News>>() {
                    @Override
                    protected List<News> doInBackground(Void... voids) {
                        return Server.getNews();
                    }
                    @Override
                    protected void onPostExecute(List<News> news) {
                        activity.application.setNews(news);
                        lodiangProgres();
                        Log.d("lodiang", "Good finished");
                    }
                }.execute();

            }
        }.execute();







        view= inflater.inflate(R.layout.loading_screen,null);
        return view;
    }

    private void lodiangProgres(){
        if(++countOfFineshedAsyncTask == 5){

            activity.afterLoading();
            Log.d("lodiang","finished");
        }

    }



}
