package zsoft.net.holiday.Fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;

public class NativeFragmentWrapper extends android.support.v4.app.Fragment {
    private  Fragment nativeFragment;


    @SuppressLint("ValidFragment")
    public NativeFragmentWrapper(Fragment nativeFragment) {
        this();
        this.nativeFragment = nativeFragment;
    }

    public NativeFragmentWrapper() {
        super();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        nativeFragment.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        nativeFragment.onActivityResult(requestCode, resultCode, data);
    }
}