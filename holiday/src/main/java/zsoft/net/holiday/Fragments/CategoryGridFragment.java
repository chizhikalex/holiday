package zsoft.net.holiday.Fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;

import zsoft.net.holiday.Caliany.Calian;
import zsoft.net.holiday.Caliany.CalianyGridAdapter;
import zsoft.net.holiday.ORM.HelperFactory;
import zsoft.net.holiday.R;
import zsoft.net.holiday.categories.Category;
import zsoft.net.holiday.categories.CategoryGridAdapter;

/**
 * Created by Sergey on 9/8/15.
 */
public class CategoryGridFragment extends Fragment {
    private View view;
    private CategoryGridAdapter adapter;
    private CalianyGridAdapter calianyGridAdapter;
    private ArrayList<Category> temp;
    private ArrayList<Category> categories= new ArrayList<>() ;
    private ArrayList<Calian> caliany = new ArrayList<>();
    GridView gvCategories;
    GridView gnCaliany;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
        view = inflater.inflate(R.layout.activity_categories,null);

        ImageView back = (ImageView) view.findViewById(R.id.back);

        ImageLoader.getInstance().displayImage("drawable://" + R.drawable.back, back);


//        Picasso.with(getActivity()).load(R.drawable.back).into((ImageView) view.findViewById(R.id.back));


         gvCategories = (GridView) view.findViewById(R.id.gvCategories);
        gnCaliany = (GridView) view.findViewById(R.id.gridViewCaliany);
        temp = new ArrayList<>();
        try {
            temp= (ArrayList) HelperFactory.getHelper().getCategoryDAO().getAllCategory();
            caliany = (ArrayList) HelperFactory.getHelper().getCalianyDAO().getAllCalians();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        sortBarCategories(false);
        adapter = new CategoryGridAdapter(getActivity(), categories);
        calianyGridAdapter =  new CalianyGridAdapter(getActivity(),caliany);

        gnCaliany.setAdapter(calianyGridAdapter);
        gnCaliany.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame,new GoodDescriptionFragment().setCalian(calianyGridAdapter.getItem(position))).addToBackStack(null).commit();

            }
        });

        gvCategories.setAdapter(adapter);

        gvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame,new GoodsGridFragment().setGoodsId(adapter.getItem(position).getId())).addToBackStack(null).commit();

            }
        });


        final ImageView ivKuhna = (ImageView) view.findViewById(R.id.ivKuhna);
        final ImageView ivBar = (ImageView) view.findViewById(R.id.ivBar);
        final ImageView ivKalian = (ImageView) view.findViewById(R.id.ivKalian);

        view.findViewById(R.id.btnKuhna).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivKuhna.setImageResource(R.drawable.kuhna_cheked);
                ivBar.setImageResource(R.drawable.bar);
                ivKalian.setImageResource(R.drawable.kalian);
                sortBarCategories(false);
                gvCategories.setVisibility(View.VISIBLE);
                gnCaliany.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
                gvCategories.deferNotifyDataSetChanged();
            }
        });

        view.findViewById(R.id.btnBar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivKuhna.setImageResource(R.drawable.kuhna);
                ivBar.setImageResource(R.drawable.bar_checked);
                ivKalian.setImageResource(R.drawable.kalian);
                gvCategories.setVisibility(View.VISIBLE);
                gnCaliany.setVisibility(View.GONE);
                sortBarCategories(true);
                adapter.notifyDataSetChanged();
                gvCategories.deferNotifyDataSetChanged();
            }
        });

        view.findViewById(R.id.btnKalian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivKuhna.setImageResource(R.drawable.kuhna);
                ivBar.setImageResource(R.drawable.bar);
                ivKalian.setImageResource(R.drawable.kalian_checked);
                gvCategories.setVisibility(View.GONE);
                gnCaliany.setVisibility(View.VISIBLE);

            }
        });


        return view;
    }

    public void sortBarCategories(boolean isBar){
        this.categories .clear();
        for(Category category : temp){
            if(category.isbar() == isBar){
                categories   .add(category);
            }
        }
    }
}
