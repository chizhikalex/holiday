package zsoft.net.holiday.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zsoft.net.holiday.MenuListFragment;
import zsoft.net.holiday.R;


public class AboutUsFragment extends Fragment {

    private View view;// fragment main view

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
        view = inflater.inflate(R.layout.activity_about_us,null);

        view.findViewById(R.id.site).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.night-museum.com.ua/#!/page_home"));
                startActivity(intent);
            }
        });

        view.findViewById(R.id.vkontakte).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://vk.com/night__museum"));
                startActivity(intent);
            }
        });

        view.findViewById(R.id.instagramm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://instagram.com/night_museum/"));
                startActivity(intent);
            }
        });

        view.findViewById(R.id.onMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, new MapsFragment()).addToBackStack(null).commit();
            }
        });



        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
//        ActionBar mActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
//        ((ImageView) mActionBar.getCustomView().findViewById(R.id.imbtn)).setImageResource(R.drawable.menu_btn);
//        mActionBar.getCustomView().findViewById(R.id.slideMenuBtn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(((MainActivity)getActivity()).mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
//                    ((MainActivity) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);
//                }else{
//                    ((MainActivity) getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
//                }
////                if (MainActivity.menu.isMenuShowing()) {
////                    MainActivity.menu.toggle(true);
////                } else {
////                    MainActivity.menu.showMenu(true);
////                }
//            }
//        });

    }

    @Override
    public void onResume() {
        super.onResume();
        MenuListFragment.setLastMenuItem(MenuListFragment.MENU_ABOUT_US);
    }

    @Override
    public void onStop() {
        super.onStop();
//        ActionBar mActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
//        ((ImageView) mActionBar.getCustomView().findViewById(R.id.imbtn)).setImageResource(R.drawable.back_arrow);
//        mActionBar.getCustomView().findViewById(R.id.slideMenuBtn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((MainActivity) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);
//                getActivity().getFragmentManager().popBackStack();
//            }
//        });
    }
}
