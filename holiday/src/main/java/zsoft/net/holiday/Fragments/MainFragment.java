package zsoft.net.holiday.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.zxing.WriterException;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKBatchRequest;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import zsoft.net.holiday.MainActivity;
import zsoft.net.holiday.MenuListFragment;
import zsoft.net.holiday.ORM.HelperFactory;
import zsoft.net.holiday.Others.OtherGridFragment;
import zsoft.net.holiday.R;
import zsoft.net.holiday.Server;
import zsoft.net.holiday.SharedPreferencesAPI;
import zsoft.net.holiday.news.News;
import zsoft.net.holiday.news.NewsGridAdapter;
import zsoft.net.holiday.present.Present;
import zsoft.net.holiday.present.PresentsGridAdapter;

/**
 * Created by Sergey on 9/7/15.
 */
public class MainFragment extends Fragment {

    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private View view;// main fragmeng view
    public MainActivity activity;
    public static CallbackManager fbcallbackManager;
    public static Dialog loginDialog;
    public static Boolean isLogining = false;
    public TextView timePicView;
    public TimePickerDialog.OnTimeSetListener timePicker = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {
            timePicView.setTextColor(Color.parseColor("#4F2A8A"));
            if(Integer.toString(i1).length()==1){
                timePicView.setText(i+":0"+i1);
                return;
            }
            timePicView.setText(i+":"+i1);
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);

        if (view != null) {
            return view;
        }
        ((MainActivity)getActivity()).getSupportActionBar().show();
        view = inflater.inflate(R.layout.activity_main, null);

        loadImages();
        initFirstPrestnView();
        initGvPresents();
        initGvNews();
        initTableBtn();

        view.findViewById(R.id.btnAllPresents).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), PresentsGridActivity.class);
//                startActivity(intent);
                activity.getFragmentManager().beginTransaction().replace(R.id.content_frame, new PresentsGridFragment()).addToBackStack(null).commit();
            }
        });

        view.findViewById(R.id.btnAllNews).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), NewsGridActivity.class);
//                startActivity(intent);
                activity.getFragmentManager().beginTransaction().replace(R.id.content_frame, new NewsGridFragment()).addToBackStack(null).commit();
            }
        });

        view.findViewById(R.id.btnMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), CategoryGridActivity.class);
//                startActivity(intent);
                activity.getFragmentManager().beginTransaction().replace(R.id.content_frame, new CategoryGridFragment()).addToBackStack(null).commit();

            }
        });

        view.findViewById(R.id.btnDiscount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog stolDialog = new Dialog(activity);
                stolDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                stolDialog.setContentView(R.layout.private_qr);
                try {
                    ((ImageView) stolDialog.findViewById(R.id.private_qr)).setImageBitmap(PresrntDescriptionFragment.encodeAsBitmap("ha-ha-ha"));
                } catch (WriterException e) {
                    e.printStackTrace();
                }
                stolDialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        stolDialog.cancel();
                    }
                });
                stolDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                stolDialog.show();
                //Toast.makeText(getActivity().getApplicationContext(), "Схема работы бонусной системы не определена", Toast.LENGTH_SHORT).show();
            }
        });

        view.findViewById(R.id.btnMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), MapsActivity.class);
//                startActivity(intent);
                activity.getFragmentManager().beginTransaction().replace(R.id.content_frame, new MapsFragment()).addToBackStack(null).commit();
                //((MainActivity) getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new MapsFragment()).addToBackStack(null).commit();
            }
        });

        view.findViewById(R.id.btnAllOther).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.getFragmentManager().beginTransaction().replace(R.id.content_frame, new OtherGridFragment()).addToBackStack(null).commit();
            }
        });


        btnCall();


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        MenuListFragment.setLastMenuItem(MenuListFragment.MENU_HOME);


        ActionBar mActionBar = activity.getSupportActionBar();
        ((ImageView) mActionBar.getCustomView().findViewById(R.id.imbtn)).setImageResource(R.drawable.menu_btn);
        mActionBar.getCustomView().findViewById(R.id.slideMenuBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    activity.mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    activity.mDrawerLayout.openDrawer(GravityCompat.START);
                }
//                if (MainActivity.menu.isMenuShowing()) {
//                    MainActivity.menu.toggle(true);
//                } else {
//                    MainActivity.menu.showMenu(true);
//                }
            }
        });
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void[] objects) {
//                try {
////                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                return null;
            }

            @Override
            protected void onPostExecute(Void o) {
                super.onPostExecute(o);
                showLogin();
            }
        }.execute();



    }

    @Override
    public void onPause() {
        super.onPause();
        if(loginDialog!=null){
            loginDialog.cancel();
        }

        MenuListFragment.setLastMenuItem("");

        ActionBar mActionBar = activity.getSupportActionBar();
        ((ImageView) mActionBar.getCustomView().findViewById(R.id.imbtn)).setImageResource(R.drawable.back_arrow);
        Log.d("123124141","");
        mActionBar.getCustomView().findViewById(R.id.slideMenuBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.mDrawerLayout.closeDrawer(GravityCompat.START);
                activity.getFragmentManager().popBackStack();
            }
        });
    }


    private void initTableBtn() {
        view.findViewById(R.id.btnStol).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog stolDialog = new Dialog(activity);
                stolDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                stolDialog.setContentView(R.layout.table_dialog);
                stolDialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        stolDialog.cancel();
                    }
                });
                stolDialog.findViewById(R.id.timePicker).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        timePicView =  (TextView)stolDialog.findViewById(R.id.editText2);
                        new TimePickerDialog(getActivity(),timePicker,22,0,true).show();
                    }
                });
                ((EditText) stolDialog.findViewById(R.id.editText3)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        int count = 0;
                        for (int j = 0, len = charSequence.toString().length(); j < len; j++) {
                            if (Character.isDigit(charSequence.toString().charAt(j))) {
                                count++;
                            }
                        }
                        if(count>=10&&count<=13){
                            ((Button) stolDialog.findViewById(R.id.btnAccept)).setEnabled(true);
                        }else{
                            ((Button) stolDialog.findViewById(R.id.btnAccept)).setEnabled(false);

                        }

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                stolDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                stolDialog.show();
            }
        });
    }

    private void showLogin() {

        Log.d("showlogin", SharedPreferencesAPI.getId() + "");
        if ((SharedPreferencesAPI.getId() != -1)) {


            return;
        }
        activity.mDrawerLayout.closeDrawer(GravityCompat.START);
        if(loginDialog!=null){
        loginDialog.cancel();}
        loginDialog = new Dialog(activity);
        loginDialog .setCanceledOnTouchOutside(false);
        loginDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                   // dialog.cancel();
                    return true;
                }
                return false;
            }
        });
        final Fragment fragment = this;
        final JSONObject objectFB = new JSONObject();
        View.OnClickListener onClickLoginDialog = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (view.getId() == R.id.pleaseLogIn) {

                }

                if (view.getId() == R.id.btnAccept) {
                    VKSdk.login(fragment, null);
                    LoginManager.getInstance().logOut();
                }
                if (view.getId() == R.id.btnCancel) {
                    fbcallbackManager = CallbackManager.Factory.create();
                    LoginManager.getInstance().logOut();
                    LoginManager.getInstance().registerCallback(fbcallbackManager,
                            new FacebookCallback<LoginResult>() {
                                @Override
                                public void onSuccess(final LoginResult loginResult) {
                                    Log.d("Login", "Success");
                                    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                            new GraphRequest.GraphJSONObjectCallback() {
                                                @Override
                                                public void onCompleted(
                                                        final JSONObject object,
                                                        GraphResponse response) {
                                                    try {
                                                        int userFaceBookId = object.getInt("id");
                                                        Bundle params = new Bundle();
                                                        params.putBoolean("redirect", false);
                                                        new GraphRequest(
                                                                AccessToken.getCurrentAccessToken(),
                                                                "/me/picture",
                                                                params,
                                                                HttpMethod.GET,
                                                                new GraphRequest.Callback() {
                                                                    public void onCompleted(GraphResponse response) {
                                                                        try {
                                                                            String birthday = "";
                                                                            try {
                                                                                birthday = object.getString("birthday");
                                                                            } catch (Exception e) {
                                                                            }
                                                                            ImageLoader.getInstance().displayImage(response.getJSONObject().getJSONObject("data").getString("url"), (ImageView) MenuListFragment.view.findViewById(R.id.ivLogo));
                                                                            SharedPreferencesAPI.saveAuth(object.getInt("id"), response.getJSONObject().getJSONObject("data").getString("url"), object.getString("name"), birthday, "fb");
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                }
                                                        ).executeAsync();


                                                        //
                                                        ((TextView) MenuListFragment.view.findViewById(R.id.textView)).setText(object.getString("name"));
                                                        String birthday = "";
                                                        try {
                                                            birthday = object.getString("birthday");
                                                        } catch (Exception e) {
                                                        }
                                                        objectFB.put("id", object.getInt("id"));
                                                        objectFB.put("name", object.getString("name"));
                                                        objectFB.put("birthday", birthday);
                                                        Log.d("", object.toString());

                                                        loginDialog.cancel();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                    Bundle parameters = new Bundle();
                                    parameters.putString("fields", "id,name,birthday");
                                    request.setParameters(parameters);
                                    request.executeAsync();
                                }

                                @Override
                                public void onCancel() {
                                    Log.d("LoginFB", "Cancel");
                                    Toast.makeText(getActivity(), "Login Cancel", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onError(FacebookException exception) {
                                    Log.d("LoginFB", "Error");
                                    exception.printStackTrace();

                                    //Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                    LoginManager.getInstance().logInWithReadPermissions(new NativeFragmentWrapper(fragment), Arrays.asList("public_profile", "user_friends"));
                }
                if (view.getId() == R.id.btnRegister) {
                    MainActivity.activity.getFragmentManager().beginTransaction().replace(R.id.content_frame, new RegistrationFragment()).addToBackStack(null).commit();
                }
                if (!objectFB.isNull("id")) {

                    try {
                        SharedPreferencesAPI.saveAuth(objectFB.getInt("id"), "http://graph.facebook.com/" + objectFB.getString("id") + "/picture", objectFB.getString("name"), objectFB.getString("birthday"), "fb");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                loginDialog.cancel();
            }
        };

        loginDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loginDialog.setContentView(R.layout.login_dialog);
        loginDialog.findViewById(R.id.btnAccept).setOnClickListener(onClickLoginDialog);
        loginDialog.findViewById(R.id.btnCancel).setOnClickListener(onClickLoginDialog);
        //loginDialog.findViewById(R.id.btnOdnoklassniki).setOnClickListener(onClickLoginDialog);onTextChanged
        loginDialog.findViewById(R.id.btnRegister).setOnClickListener(onClickLoginDialog);
        loginDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loginDialog.show();

    }

    private void initGvNews() {
        ArrayList<News> news = new ArrayList<>();
        try {
            news = (ArrayList) HelperFactory.getHelper().getNewsDAO().getAllNews();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ViewGroup.LayoutParams layoutParams = view.findViewById(R.id.gvNews).getLayoutParams();

        if (news.size()>=3) {

        }else{if(news.size()==0){
            view.findViewById(R.id.gvNews).setVisibility(View.GONE);
            view.findViewById(R.id.rlNews).setVisibility(View.GONE);
        }else {
            layoutParams.height = convertDpToPixels( 154, getActivity());; //this is in pixels
            view.findViewById(R.id.gvNews).setLayoutParams(layoutParams);
        }

        }
        final GridView gvNews = (GridView) view.findViewById(R.id.gvNews);
        final NewsGridAdapter adapter = new NewsGridAdapter(activity, news);
        gvNews.setAdapter(adapter);

        gvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {

                ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        activity.getFragmentManager().beginTransaction().replace(R.id.content_frame,
                                new NewsDescriptionFragment().setNews(adapter.getItem(position))).addToBackStack(null).commit();
                    }
                }, 250);

            }
        });

    }

    private void initGvPresents() {
        ArrayList<Present> presents = new ArrayList<Present>();
        try {
            presents = (ArrayList) HelperFactory.getHelper().getPresentDAO().getAllPresents();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final GridView gvPresents = (GridView) view.findViewById(R.id.gvPresents);
        final PresentsGridAdapter adapter = new PresentsGridAdapter(activity, presents);

        gvPresents.setAdapter(adapter);
        gvPresents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        activity.getFragmentManager().beginTransaction().replace(R.id.content_frame,
                                new PresrntDescriptionFragment().setPresent(adapter.getItem(position))).addToBackStack(null).commit();
                    }
                }, 250);


            }
        });

    }

    private void initFirstPrestnView() {
        final Present firstPresent = SharedPreferencesAPI.getFirstPresent();
        if (firstPresent == null) {
            return;
        }
        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir(Server.PRESENTS, Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, firstPresent.getImagepath());
        Picasso.with(getActivity()).load(mypath).into((ImageView) view.findViewById(R.id.ivFirstPresent));
        //imageLoader.displayImage(firstPresent.getImagepath(), (ImageView) view.findViewById(R.id.ivFirstPresent), options);
        ((TextView) view.findViewById(R.id.tvFirstPresent)).setText(firstPresent.getTitle());
        ((TextView) view.findViewById(R.id.tvFirstPresntPrice)).setText(firstPresent.getPrice() + "");

        view.findViewById(R.id.btnFirstPresent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        activity.getFragmentManager().beginTransaction().replace(R.id.content_frame,
                                new PresrntDescriptionFragment().setPresent(firstPresent)).addToBackStack(null).commit();
                    }
                }, 250);

            }
        });
    }

    private void btnCall() {
        view.findViewById(R.id.btnCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialog = getActivity().getLayoutInflater().inflate(R.layout.call_dialog, null);

                final Dialog callDialog = new Dialog(getActivity());
                callDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                callDialog.setContentView(dialog);
                callDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                callDialog.show();

                dialog.findViewById(R.id.btn099).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String uri = "tel:+380991777244";
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse(uri));
                        startActivity(intent);
                    }
                });

                dialog.findViewById(R.id.btn093).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String uri = "tel:+380930441674";
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse(uri));
                        startActivity(intent);
                    }
                });

                dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callDialog.cancel();
                    }
                });

            }
        });
    }

    private void loadImages() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity)
                .build();
        ImageLoader.getInstance().init(config);
        options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(false)  // default
                .delayBeforeLoading(0)
                .cacheInMemory(true) // default
                .cacheOnDisk(true) // default
                .build();
        imageLoader = ImageLoader.getInstance();

        ImageView ivLogo = (ImageView) view.findViewById(R.id.ivLogo);
        ImageView ivDisount = (ImageView) view.findViewById(R.id.ivDisount);
        ImageView ivMenu = (ImageView) view.findViewById(R.id.ivMenu);
        ImageView ivStol = (ImageView) view.findViewById(R.id.ivStol);
        ImageView ivWeOnMap = (ImageView) view.findViewById(R.id.ivWeOnMap);
        ImageView ivCallUs = (ImageView) view.findViewById(R.id.ivCallUs);
        ImageView ivBackground = (ImageView) view.findViewById(R.id.ivBackground);


//        Picasso.with(getActivity()).load(R.drawable.logo).into(ivLogo);
//        Picasso.with(getActivity()).load(R.drawable.qr).into(ivDisount);
//        Picasso.with(getActivity()).load(R.drawable.blini).into(ivMenu);
//        Picasso.with(getActivity()).load(R.drawable.table).into(ivStol);
//        Picasso.with(getActivity()).load(R.drawable.we_on_map).into(ivWeOnMap);
//        Picasso.with(getActivity()).load(R.drawable.call_us).into(ivCallUs);
//        Picasso.with(getActivity()).load(R.drawable.back).into(ivBackground);


        imageLoader.displayImage("drawable://" + R.drawable.logo, ivLogo);
        imageLoader.displayImage("drawable://" + R.drawable.qr, ivDisount);
        imageLoader.displayImage("drawable://" + R.drawable.blini, ivMenu);
        imageLoader.displayImage("drawable://" + R.drawable.table, ivStol);
        imageLoader.displayImage("drawable://" + R.drawable.we_on_map, ivWeOnMap);
        imageLoader.displayImage("drawable://" + R.drawable.call_us, ivCallUs);
        imageLoader.displayImage("drawable://" + R.drawable.back, ivBackground);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (fbcallbackManager != null) {
            fbcallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
// Пользователь успешно авторизовался
                res.saveTokenToSharedPreferences(getActivity(), "token");
                Log.d("token", res.email + "" + res.userId + "" + res.accessToken);
                VKRequest request = VKApi.users().get(VKParameters.from(res.userId));
                VKRequest yourRequest = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "photo_100", "bdate"));
                VKBatchRequest batch = new VKBatchRequest(request, yourRequest);
                batch.executeWithListener(new VKBatchRequest.VKBatchRequestListener() {

                    @Override
                    public void onComplete(VKResponse[] responses) {
                        super.onComplete(responses);

                        try {
                            JSONObject object = new JSONObject(responses[0].responseString);
                            JSONObject ob = object.getJSONArray("response").getJSONObject(0);
                            Log.d("", ob.toString());
                            ((TextView) MenuListFragment.view.findViewById(R.id.textView)).setText(ob.getString("first_name") + " " + ob.getString("last_name"));
                            object = new JSONObject(responses[1].responseString);
                            JSONObject obj = object.getJSONArray("response").getJSONObject(0);
                            Log.d("", obj.toString());
                            ImageLoader.getInstance().displayImage(obj.getString("photo_100"), (ImageView) MenuListFragment.view.findViewById(R.id.ivLogo));
                            String bday = "";
                            try {
                                bday = obj.getString("bdate");
                            } catch (Exception e) {
                            }
                            SharedPreferencesAPI.saveAuth(ob.getInt("id"), obj.getString("photo_100"), ob.getString("first_name") + " " + ob.getString("last_name"), bday, "vk");
                            loginDialog.cancel();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        super.onError(error);

                        // isApdating=false;
                    }
                });
            }

            @Override
            public void onError(VKError error) {
// Произошла ошибка авторизации (например, пользователь запретил авторизацию)
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);

        }

    }

    public static int convertDpToPixels(float dp, Context context){
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }

}
