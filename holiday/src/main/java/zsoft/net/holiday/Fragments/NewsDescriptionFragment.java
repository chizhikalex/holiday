package zsoft.net.holiday.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.Serializable;

import zsoft.net.holiday.App;
import zsoft.net.holiday.InfoFragment;
import zsoft.net.holiday.MainActivity;
import zsoft.net.holiday.R;
import zsoft.net.holiday.Server;
import zsoft.net.holiday.news.News;

/**
 * Created by Sergey on 9/8/15.
 */
public class NewsDescriptionFragment extends Fragment {
    private View view;
    private App application;
    public Serializable getNews() {
        return news;
    }

    public NewsDescriptionFragment setNews(Serializable news) {
        this.news = (News)news;
        return this;
    }
    private News news;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_news_description,null);
       // News neww = (News) getIntent().getSerializableExtra("neww");
        view.findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getFragmentManager().popBackStack();
            }
        });

        application = ((App) getActivity().getApplication());
        if(android.os.Build.VERSION.SDK_INT>=21) {
            application.colorizeInfoBar(getActivity());
        }

        news = (News) getNews();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .memoryCacheExtraOptions(320, 640) // default = device screen dimensions
                .diskCacheExtraOptions(320, 640, null)
                .threadPoolSize(4) // default
                .build();
        ImageLoader.getInstance();//;//.init(config);
        ImageLoader imageLoader = ImageLoader.getInstance();


        ImageView ivImage = (ImageView) view.findViewById(R.id.ivImage);
        TextView tvWhen = (TextView) view.findViewById(R.id.tvWhen);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);



        //imageLoader.displayImage(news.getImagepath(), ivImage);

        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir(Server.NEWS, Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,news.getImagepath());
        Picasso.with(getActivity()).load(mypath).into(ivImage);

        //imageLoader.displayImage("file://"+mypath, ivImage);

        tvWhen.setText(news.getWhen());
        tvTitle.setText(news.getTitle());
        tvDescription.setText(news.getDescription());


        view.findViewById(R.id.infoBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    ((MainActivity)getActivity()).mDrawerLayout.openDrawer(Gravity.RIGHT);

            }
        });
        settingInfoFragment();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
//        menu.setSlidingEnabled(true);
        MainActivity.isDescriptionScreen=true;
        ((MainActivity)getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.RIGHT);
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
//        if(menu.isMenuShowing()){
//            menu.toggle();
//        }
        //menu.setSlidingEnabled(false);
        MainActivity.isDescriptionScreen=false;
        ((MainActivity)getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

    void settingInfoFragment(){
        ((TextView)InfoFragment.view.findViewById(R.id.textView25)).setText(((News) news).getDescription());
//        if(((Present)present).get()==null||moredescription.length()==0){
        InfoFragment.view.findViewById(R.id.textView26).setVisibility(View.GONE);
        InfoFragment.view.findViewById(R.id.textView27).setVisibility(View.GONE);
//        }else{
//            ((TextView)  view.findViewById(R.id.textView27)).setText(moredescription);
//        }
    }
}
