package zsoft.net.holiday.Fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.io.File;

import zsoft.net.holiday.App;
import zsoft.net.holiday.Caliany.Calian;
import zsoft.net.holiday.InfoFragment;
import zsoft.net.holiday.MainActivity;
import zsoft.net.holiday.R;
import zsoft.net.holiday.Server;
import zsoft.net.holiday.goods.Good;

/**
 * Created by Sergey on 9/8/15.
 */
public class GoodDescriptionFragment extends Fragment {
    private View view;
    private App application;
    public Good getGood() {
        return good;
    }

    public Calian getCalian() {
        return calian;
    }

    public GoodDescriptionFragment setCalian(Calian calian) {
        this.calian = calian;
        return this;
    }

    private Calian calian;

    public GoodDescriptionFragment setGood(Good good) {
        this.good = good;
        return this;
    }
    private Good good;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }

       view = inflater.inflate(R.layout.activity_good_description,null);

        view.findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getFragmentManager().popBackStack();
            }
        });

        view.findViewById(R.id.btn_get).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                View dialog = getActivity().getLayoutInflater().inflate(R.layout.private_qr, null);

                final Dialog callDialog = new Dialog(getActivity());
                callDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                callDialog.setContentView(dialog);
                callDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                callDialog.show();
                dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callDialog.cancel();
                    }
                });
                 ((TextView) dialog.findViewById(R.id.description)).setText("Покажите это официанту и он принесет вам подарок!");
                ImageView imageView = (ImageView) dialog.findViewById(R.id.private_qr);
                try {
                    Bitmap bitmap = PresrntDescriptionFragment.encodeAsBitmap(((Good) good).getId() + "");
                    imageView.setImageBitmap(bitmap);
                } catch (WriterException e) {
                    e.printStackTrace();
                }
//                ((Button)view.findViewById(R.id.btn_get)).setText("ГОТОВО");
//                view.findViewById(R.id.btn_get).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        FragmentManager fragmentManager =  getActivity().getFragmentManager();
//                        if (fragmentManager.getBackStackEntryCount() > 0) {
//                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        }
//                    }
//                });
                //view.findViewById(R.id.qr_code).setVisibility(View.VISIBLE);

            }
        });

        application = ((App) getActivity().getApplication());
        if(android.os.Build.VERSION.SDK_INT>=21) {
            application.colorizeInfoBar(getActivity());
        }

        Good good = (Good) getGood();
        if(calian!=null){
            good=new Good();
            good.setMoredescription(calian.getMoredescription());
            good.setDescription(calian.getDescription());
            good.setId(calian.getId());
            good.setImagepath(calian.getImagepath());
            good.setPrice(calian.getPrice());
            good.setTitle(calian.getTitle());
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .memoryCacheExtraOptions(320, 640) // default = device screen dimensions
                .diskCacheExtraOptions(320, 640, null)
                .threadPoolSize(4) // default
                .build();
        ImageLoader.getInstance();//;//.init(config);
        ImageLoader imageLoader = ImageLoader.getInstance();


        ImageView ivImage = (ImageView) view.findViewById(R.id.ivImage);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);



        ContextWrapper cw = new ContextWrapper(getActivity());

        File directory ;
        if(calian!=null){
            directory =  cw.getDir(Server.CALIANY, Context.MODE_PRIVATE);
        }else{
            directory = cw.getDir(Server.GOODS, Context.MODE_PRIVATE);
        }

        // Create imageDir
        File mypath=new File(directory,good.getImagepath());
        Picasso.with(getActivity()).load(mypath).into(ivImage);
        //imageLoader.displayImage("file://"+mypath, ivImage);

        tvPrice.setText(good.getPrice() + "");
        tvTitle.setText(good.getTitle());
        tvDescription.setText(good.getDescription());



        view.findViewById(R.id.infoBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                   ((MainActivity)getActivity()).mDrawerLayout.openDrawer(Gravity.RIGHT);

            }
        });
        settingInfoFragment();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
//        menu.setSlidingEnabled(true);
        MainActivity.isDescriptionScreen=true;
        ((MainActivity)getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.RIGHT);
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();


    }

    @Override
    public void onStop() {
        super.onStop();
//        if(menu.isMenuShowing()){
//            menu.toggle();
//        }
//        menu.setSlidingEnabled(false);
        MainActivity.isDescriptionScreen=false;
        ((MainActivity)getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

    void settingInfoFragment(){

        if(calian!=null){
                 good=new Good();
                good.setMoredescription(calian.getMoredescription());
                good.setDescription(calian.getDescription());
                good.setId(calian.getId());
                good.setImagepath(calian.getImagepath());
                good.setPrice(calian.getPrice());
                good.setTitle(calian.getTitle());
        }
        ((TextView)InfoFragment.view .findViewById(R.id.textView25)).setText(((Good) good).getDescription());
        if(((Good)good).getMoredescription()==null||((Good)good).getMoredescription().length()==0){
            InfoFragment.view .findViewById(R.id.textView26).setVisibility(View.GONE);
            InfoFragment.view .findViewById(R.id.textView27).setVisibility(View.GONE);
        }else{
            InfoFragment.view .findViewById(R.id.textView26).setVisibility(View.VISIBLE);
            InfoFragment.view .findViewById(R.id.textView27).setVisibility(View.VISIBLE);
            ((TextView)  InfoFragment.view .findViewById(R.id.textView27)).setText(((Good)good).getMoredescription());
        }
    }
}
