package zsoft.net.holiday;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Dobromir on 02.07.2015.
 */
public final class AboutUsActivity extends AppCompatActivity {

    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        application = ((App) getApplication());
       // application.setCustomActionBar(this);

    }
}
