package zsoft.net.holiday.goods;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import zsoft.net.holiday.App;
import zsoft.net.holiday.R;

/**
 * Created by Dobromir on 04.07.2015.
 */
public class GoodsGridActivity extends AppCompatActivity {

    private GoodsGridAdapter adapter;
    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods);

       int id = getIntent().getIntExtra("id", 0);

        application = ((App) getApplication());
       // application.setCustomActionBar(this);

        GridView gvGoods = (GridView) findViewById(R.id.gvGoods);
        adapter = new GoodsGridAdapter(this, ((App)getApplication()).getGoods(id));
        gvGoods.setAdapter(adapter);

        gvGoods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), GoodDescriptionActivity.class);
                intent.putExtra("good", adapter.getItem(position));
                startActivity(intent);
            }
        });

    }
}
