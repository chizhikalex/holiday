package zsoft.net.holiday.goods;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import zsoft.net.holiday.App;
import zsoft.net.holiday.InfoFragment;
import zsoft.net.holiday.R;

/**
 * Created by Dobromir on 04.07.2015.
 */
public class GoodDescriptionActivity extends Activity {

    private App application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_description);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        application = ((App) getApplication());
        if(android.os.Build.VERSION.SDK_INT>=21) {
            application.colorizeInfoBar(this);
        }

        Good good = (Good) getIntent().getSerializableExtra("good");

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(320, 640) // default = device screen dimensions
                .diskCacheExtraOptions(320, 640, null)
                .threadPoolSize(4) // default
                .build();
        ImageLoader.getInstance();//.init(config);
        ImageLoader imageLoader = ImageLoader.getInstance();


        ImageView ivImage = (ImageView) findViewById(R.id.ivImage);
        TextView tvPrice = (TextView) findViewById(R.id.tvPrice);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) findViewById(R.id.tvDescription);

        imageLoader.displayImage(good.getImagepath(), ivImage);
        tvPrice.setText(good.getPrice() + "");
        tvTitle.setText(good.getTitle());
        tvDescription.setText(good.getDescription());


        // configure the SlidingMenu

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int behindWidth = (size.x / 2) + ((size.x / 2) / 2);

//        final SlidingMenu menu = new SlidingMenu(this);
//        menu.setMode(SlidingMenu.RIGHT);
//        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//        menu.setShadowDrawable(R.drawable.shadowright);
//        menu.setShadowWidthRes(R.dimen.shadow_width);
//        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
//        menu.setFadeDegree(0.35f);
//        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT, false);
//        menu.setMenu(R.layout.menu);

        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(R.id.menu, new InfoFragment()).commit();


        findViewById(R.id.infoBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                menu.showMenu();
            }
        });




    }
}
